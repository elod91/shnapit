package shnapit.shnapit.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.materialdrawer.model.BasePrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.utils.ViewHolderFactory;

import shnapit.shnapit.R;

public class DrawerItem extends BasePrimaryDrawerItem<DrawerItem> {
    public static final String TYPE = "DRAWER_ITEM";

    private String title, badge;

    public DrawerItem withTitle(String title) {
        this.title = title;
        return this;
    }

    public DrawerItem withBadge(String badge) {
        this.badge = badge;
        return this;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    @Override
    public ViewHolderFactory getFactory() {
        return new ItemFactory();
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.drawer_menu_item;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder holder) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.itemView.setSelected(isSelected());

        if (title != null)
            viewHolder.title.setText(title);

        if (badge == null) {
            viewHolder.badge.setVisibility(View.GONE);
            return;
        }

        viewHolder.badge.setVisibility(View.VISIBLE);
        viewHolder.badge.setText(badge);
    }

    public static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder factory(View v) {
            return new ViewHolder(v);
        }
    }

    private static class ViewHolder extends BaseViewHolder {
        private TextView title, badge;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.drawer_menu_title);
            badge = (TextView) view.findViewById(R.id.drawer_menu_badge);
        }
    }
}
