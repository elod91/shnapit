package shnapit.shnapit.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

public class ColorPickerDialog extends Dialog {

    private OnColorChangedListener onColorChangedListener;

    public ColorPickerDialog(Context context, OnColorChangedListener onColorChangedListener) {
        super(context);

        this.onColorChangedListener = onColorChangedListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(new ColorPicker(getContext(), onColorChangedListener));
    }

    public interface OnColorChangedListener {
        void colorChanged(int color);
    }
}
