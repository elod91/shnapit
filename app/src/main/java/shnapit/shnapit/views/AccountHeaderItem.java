package shnapit.shnapit.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.materialdrawer.model.BasePrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.utils.ViewHolderFactory;
import com.squareup.picasso.Picasso;

import shnapit.shnapit.R;
import shnapit.shnapit.utils.DialogFactory;

public class AccountHeaderItem extends BasePrimaryDrawerItem<AccountHeaderItem> {
    public static final String TYPE = "ACCOUNT_HEADER_ITEM";

    private Context context;
    private String fullName, userName, photoUrl;

    private OnChangeProfilePictureListener onChangeProfilePictureListener;

    public AccountHeaderItem(Context context) {
        this.context = context;
    }

    public AccountHeaderItem withFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public AccountHeaderItem withUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public AccountHeaderItem withPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }

    public AccountHeaderItem withListener(OnChangeProfilePictureListener onChangeProfilePictureListener) {
        this.onChangeProfilePictureListener = onChangeProfilePictureListener;
        return this;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public ViewHolderFactory getFactory() {
        return new ItemFactory();
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.drawer_account_header;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder holder) {
        ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.fullName.setText(fullName);
        viewHolder.userName.setText(userName);

        if (photoUrl != null)
            Picasso.with(context).load(photoUrl).into(viewHolder.photo);

        viewHolder.photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFactory.showPhotoChooserDialog(context, new DialogFactory.OnPhotoChooserListener() {
                    @Override
                    public void onActionSelected(int action) {
                        dispatchPhotoChooser(action);
                    }
                });
            }
        });
    }

    private void dispatchPhotoChooser(int action) {
        if (onChangeProfilePictureListener == null)
            return;

        switch (action) {
            case DialogFactory.OnPhotoChooserListener.ACTION_CAMERA:
                onChangeProfilePictureListener.onTakeNewPicture();
                break;

            case DialogFactory.OnPhotoChooserListener.ACTION_GALLERY:
                onChangeProfilePictureListener.onSelectFromGallery();
        }
    }

    public interface OnChangeProfilePictureListener {
        void onTakeNewPicture();

        void onSelectFromGallery();
    }

    public static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder factory(View v) {
            return new ViewHolder(v);
        }
    }

    private static class ViewHolder extends BaseViewHolder {
        private ImageView photo;
        private TextView fullName, userName;

        public ViewHolder(View view) {
            super(view);
            photo = (ImageView) view.findViewById(R.id.drawer_account_header_photo);
            fullName = (TextView) view.findViewById(R.id.drawer_account_header_full_name);
            userName = (TextView) view.findViewById(R.id.drawer_account_header_user_name);
        }
    }
}
