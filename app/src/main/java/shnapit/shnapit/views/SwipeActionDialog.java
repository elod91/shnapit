package shnapit.shnapit.views;

import android.app.Dialog;
import android.content.Context;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import shnapit.shnapit.R;


/**
 * Created by Reea163 on 1/6/2016.
 */
public class SwipeActionDialog extends Dialog {

    public SwipeActionDialog(Context context) {
        super(context, R.style.SwipeDialog);

        setContentView(R.layout.swipe_action_dialog);
        setCancelable(false);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.dimAmount = 0.6f;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    public void setTextAndImage(String text, int imageRes) {
        if (text != null)
            ((TextView) findViewById(R.id.swipe_action_text)).setText(text);

        ((ImageView) findViewById(R.id.swipe_action_image)).setImageResource(imageRes);
    }
}
