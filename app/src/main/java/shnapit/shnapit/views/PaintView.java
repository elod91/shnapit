package shnapit.shnapit.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class PaintView extends View {
    public static final int STROKE_WIDTH = 4;

    private Bitmap finishedPathHolder;
    private Canvas finishedPathCanvas;

    private Path path;
    private Paint drawPaint, helperPaint;

    private float lastX, lastY, strokeWidth;
    private int drawColor;
    private boolean active;

    public PaintView(Context context) {
        super(context);
        init(context);
    }

    public PaintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PaintView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        strokeWidth = context.getResources().getDisplayMetrics().density * STROKE_WIDTH + 0.5f;
        drawColor = Color.BLACK;

        path = new Path();

        active = false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT);
        if (finishedPathHolder != null)
            canvas.drawBitmap(finishedPathHolder, 0, 0, getHelperPaint());
        canvas.drawPath(path, getDrawPaint());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (active) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startDrawing(event.getX(), event.getY());
                    break;

                case MotionEvent.ACTION_MOVE:
                    continueDrawing(event.getX(), event.getY());
                    break;

                case MotionEvent.ACTION_UP:
                    finishDrawing();
            }
            invalidate();
        }

        return true;
    }

    private void startDrawing(float x, float y) {
        path.moveTo(x, y);

        lastX = x;
        lastY = y;
    }

    private void continueDrawing(float x, float y) {
        path.quadTo(lastX, lastY, (lastX + x) / 2, (lastY + y) / 2);

        lastX = x;
        lastY = y;
    }

    private void finishDrawing() {
        path.lineTo(lastX, lastY);

        addToFinished();

        path.reset();
    }

    private void addToFinished() {
        if (finishedPathHolder == null)
            finishedPathHolder = Bitmap.createBitmap(getWidth(), getWidth(), Bitmap.Config.ARGB_8888);

        if (finishedPathCanvas == null)
            finishedPathCanvas = new Canvas(finishedPathHolder);

        finishedPathCanvas.drawPath(path, getDrawPaint());
    }

    private Paint getDrawPaint() {
        if (drawPaint == null) {
            drawPaint = new Paint();
            drawPaint.setAntiAlias(true);
            drawPaint.setDither(true);
            drawPaint.setStyle(Paint.Style.STROKE);
            drawPaint.setStrokeJoin(Paint.Join.ROUND);
            drawPaint.setStrokeCap(Paint.Cap.ROUND);
            drawPaint.setStrokeWidth(strokeWidth);
        }

        drawPaint.setColor(drawColor);
        return drawPaint;
    }

    private Paint getHelperPaint() {
        if (helperPaint == null)
            helperPaint = new Paint(Paint.DITHER_FLAG);

        return helperPaint;
    }

    public void undoChanges() {
        finishedPathHolder = Bitmap.createBitmap(getWidth(), getWidth(), Bitmap.Config.ARGB_8888);
        finishedPathCanvas = new Canvas(finishedPathHolder);
        path.reset();

        invalidate();
    }

    public void setDrawColor(int color) {
        drawColor = color;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
