package shnapit.shnapit.views;


import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class HashEditText extends EditText {

    private HashBackPressListener hashBackPressListener;

    public HashEditText(Context context) {
        super(context);
    }

    public HashEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HashEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && hashBackPressListener != null)
            hashBackPressListener.onHashBackPress();

        return true;
    }

    public void setHashBackPressListener(HashBackPressListener hashBackPressListener) {
        this.hashBackPressListener = hashBackPressListener;
    }

    public interface HashBackPressListener {
        void onHashBackPress();
    }
}
