package shnapit.shnapit.rest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.ResponseBody;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import shnapit.shnapit.R;
import shnapit.shnapit.rest.responses.ErrorResponse;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.SignatureUtil;

public class RestClient {

    private static RestClient INSTANCE;

    private Retrofit retrofit;

    private RestClient(String baseUrl) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.000Z'")
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static synchronized RestClient getInstance() {
        if (INSTANCE == null)
            INSTANCE = new RestClient(RestConstants.BASE_URL);

        return INSTANCE;
    }

    public static synchronized RestClient getInstance(String baseUrl) {
        if (INSTANCE == null)
            INSTANCE = new RestClient(baseUrl);

        return INSTANCE;
    }

    public static boolean hasNetworkConnection(Context context) {
        return hasNetworkConnection(context, false);
    }

    public static boolean hasNetworkConnection(Context context, boolean showDialog) {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            boolean hasNetwork = (activeNetwork != null) && activeNetwork.isConnectedOrConnecting();
            if (!hasNetwork && showDialog)
                showNoInternetDialog(context);

            return hasNetwork;
        }
        return false;
    }

    private static void showNoInternetDialog(Context context) {
        DialogFactory.showDialogOneButton(context, context.getString(R.string.dialog_error_title), context.getString(R.string.dialog_no_internet_message), null);
    }

    public static String getSignature(Context context, String apiToken, String endPoint) {
        long serverTimestamp = PersistentUtils.getLong(context, PersistentUtils.SERVER_TIMESTAMP, System.currentTimeMillis() / 1000);
        long deviceTimestamp = PersistentUtils.getLong(context, PersistentUtils.DEVICE_TIMESTAMP, System.currentTimeMillis() / 1000);

        return SignatureUtil.getSignature(apiToken, endPoint, serverTimestamp + System.currentTimeMillis() / 1000 - deviceTimestamp);
    }

    public static String getSignatureForUpload(String apiToken, String endPoint) {
        return SignatureUtil.getSignature(apiToken, endPoint);
    }

    public static void handleErrors(Context context, ResponseBody responseBody, DialogFactory.OnButtonPressedListener onButtonPressedListener) {
        try {
            String errorJson = responseBody.string();
            ErrorResponse errorResponse = new Gson().fromJson(errorJson, ErrorResponse.class);

            if (errorResponse == null) {
                DialogFactory.showDialogOneButton(context, context.getString(R.string.dialog_error_title), context.getString(R.string.dialog_general_error_message), onButtonPressedListener);
                return;
            }

            DialogFactory.showDialogOneButton(context, errorResponse.getError().getTitle(), errorResponse.getError().getMessages().get(0), onButtonPressedListener);
        } catch (Exception e) {
            DialogFactory.showDialogOneButton(context, context.getString(R.string.dialog_error_title), context.getString(R.string.dialog_general_error_message), onButtonPressedListener);
            e.printStackTrace();
        }
    }

    public static void handleErrors(Context context, ResponseBody responseBody) {
        handleErrors(context, responseBody, null);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
