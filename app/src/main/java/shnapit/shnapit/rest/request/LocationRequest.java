package shnapit.shnapit.rest.request;

import com.google.gson.annotations.Expose;

public class LocationRequest {

    @Expose
    int allowed;

    @Expose
    double latitude;

    @Expose
    double longitude;

    @Expose
    String signature;

    public LocationRequest(int allowed, String signature) {
        this.allowed = allowed;
        this.signature = signature;
    }

    public LocationRequest(int allowed, double latitude, double longitude, String signature) {
        this.allowed = allowed;
        this.latitude = latitude;
        this.longitude = longitude;
        this.signature = signature;
    }

    public int getAllowed() {
        return allowed;
    }

    public void setAllowed(int allowed) {
        this.allowed = allowed;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
