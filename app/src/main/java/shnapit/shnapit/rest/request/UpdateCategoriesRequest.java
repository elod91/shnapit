package shnapit.shnapit.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateCategoriesRequest {

    @Expose
    @SerializedName("first_name")
    String firstName;

    @Expose
    @SerializedName("last_name")
    String lastName;

    @Expose
    @SerializedName("category_ids")
    String categoryIds;

    @Expose
    String signature;

    public UpdateCategoriesRequest(String firstName, String lastName, String categoryIds, String signature) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.categoryIds = categoryIds;
        this.signature = signature;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(String categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
