package shnapit.shnapit.rest;

public class RestConstants {
    public static final String UPLOAD_HASH_KEY = "2a3e3d9d0ef158a4733cd6e64bc7b6a0d4ea90074ae726753b0770b4573440f3";

    //    public static final String BASE_URL = "http://staging.shnapit.com";
    public static final String BASE_URL = "http://api.shnapit.com";
    public static final String TIMESTAMP = "/api/timestamp";

    /*
    USER RELATED URLS
     */
    public static final String USER_LOGIN = "/api/login.json";
    public static final String USER_REGISTER = "/api/signup.json";
    public static final String USER_FORGOT_PASSWORD = "/api/forgot_password.json";
    public static final String USER_VALIDATE_USERNAME = "/api/validate_user_name.json";
    public static final String USER_SEND_LOCATION = "/api/{api_token}/set_location.json";
    public static final String UPDATE_USER = "/api/{api_token}/update_user.json";
    public static final String DELETE_USER = "/api/{api_token}/delete_user.json";
    public static final String GET_CATEGORIES = "/api/{api_token}/categories.json";
    public static final String FACEBOOK_LOGIN = "/api/users";

    /*
     PRODUCT RELATED URLS
     */
    public static final String PRODUCT_GENERAL = "/api/{api_token}/feeds";
    public static final String PRODUCT_TIMELINE = PRODUCT_GENERAL + "/timeline.json";
    public static final String PRODUCT_WANT = "/api/{api_token}/feeds/{id}/want.json";
    public static final String PRODUCT_REMOVE = PRODUCT_GENERAL + "/{id}.json";
    public static final String PRODUCT_SEE = PRODUCT_GENERAL + "/{id}/see.json";
    public static final String PRODUCT_RESHARE = PRODUCT_GENERAL + "/{id}/share.json";

    /*
    CONNECTIONS RELATED URLS
     */
    public static final String CONNECTIONS_GENERAL = "/api/{api_token}/friends.json";
    public static final String SEARCH_USERS = "/api/{api_token}/search_for_users.json";
    public static final String DELETE_CONNECTION = "/api/{api_token}/delete_friendship.json";
    public static final String ACCEPT_REQUEST = "/api/{api_token}/update_friendship.json";
}
