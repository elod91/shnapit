package shnapit.shnapit.rest.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import shnapit.shnapit.database.models.Product;

public class ProductListResponse {

    @Expose
    @SerializedName("feeds")
    List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
