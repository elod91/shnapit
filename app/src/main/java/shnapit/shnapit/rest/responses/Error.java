package shnapit.shnapit.rest.responses;

import com.google.gson.annotations.Expose;

import java.util.List;

public class Error {

    @Expose
    List<String> messages;

    @Expose
    int code;

    @Expose
    String title;

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Error{" +
                "messages=" + messages +
                ", code=" + code +
                ", title='" + title + '\'' +
                '}';
    }
}
