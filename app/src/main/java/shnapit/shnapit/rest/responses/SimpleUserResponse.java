package shnapit.shnapit.rest.responses;

import com.google.gson.annotations.Expose;

import shnapit.shnapit.database.models.User;

public class SimpleUserResponse {

    @Expose
    User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
