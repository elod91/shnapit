package shnapit.shnapit.rest.responses;

import com.google.gson.annotations.Expose;

import java.util.List;

import shnapit.shnapit.database.models.User;

public class SimpleMultipleUserResponse {

    @Expose
    List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
