package shnapit.shnapit.rest.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import shnapit.shnapit.database.models.User;

public class ConnectionsResponse {

    @Expose
    @SerializedName("accepted_friends")
    List<User> acceptedConnections;

    @Expose
    @SerializedName("pending_friends")
    List<User> pendingConnections;

    @Expose
    @SerializedName("pending_friend_requests")
    List<User> pendingConnectionRequests;

    public List<User> getAcceptedConnections() {
        return acceptedConnections;
    }

    public List<User> getPendingConnections() {
        return pendingConnections;
    }

    public List<User> getPendingConnectionRequests() {
        return pendingConnectionRequests;
    }
}
