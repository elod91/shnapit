package shnapit.shnapit.rest.responses;

import com.google.gson.annotations.Expose;

public class TimestampResponse {

    @Expose
    long timestamp;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
