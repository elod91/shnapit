package shnapit.shnapit.rest.responses;

import com.google.gson.annotations.Expose;

public class Category {
    @Expose
    long id;

    @Expose
    String name;

    @Expose
    boolean selected;

    public Category(long id, String name, boolean selected) {
        this.id = id;
        this.name = name;
        this.selected = selected;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
