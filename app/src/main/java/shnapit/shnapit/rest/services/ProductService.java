package shnapit.shnapit.rest.services;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import shnapit.shnapit.database.models.Product;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.responses.ProductListResponse;

public interface ProductService {

    @Multipart
    @POST(RestConstants.PRODUCT_GENERAL + ".json")
    Call<Product> createProduct(@Path("api_token") String apiToken, @Part("photo\"; filename=\"feed.png") RequestBody image, @Part("signature") RequestBody signature, @Part("keywords") RequestBody keywords, @Part("category_id") RequestBody categoryId, @Part("brand") RequestBody productBrand, @Part("name") RequestBody productName, @Part("price") RequestBody productPrice, @Part("link") RequestBody productWebsite);

    @Multipart
    @POST(RestConstants.PRODUCT_GENERAL + "/{product_id}.json")
    Call<Product> updateProduct(@Path("api_token") String apiToken, @Path("product_id") long productId, @Part("photo\"; filename=\"feed.png") RequestBody image, @Part("signature") RequestBody signature, @Part("keywords") RequestBody keywords, @Part("category_id") RequestBody categoryId, @Part("brand") RequestBody productBrand, @Part("name") RequestBody productName, @Part("price") RequestBody productPrice, @Part("link") RequestBody productWebsite, @Part("user_ids") RequestBody userIds);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.PRODUCT_RESHARE)
    Call<Object> reshareProduct(@Path("api_token") String apiToken, @Path("id") long productId, @Query("share_on") String shareOn, @Query("user_ids[]") List<Long> userIds, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @GET(RestConstants.PRODUCT_TIMELINE)
    Call<ProductListResponse> getProductsForSaved(@Path("api_token") String apiToken, @Query("limit") Integer limit, @Query("offset") Integer offset, @Query("user_id") Long userId, @Query("keyword") String keyword, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @GET(RestConstants.PRODUCT_GENERAL + ".json")
    Call<ProductListResponse> getProductsForDiscovery(@Path("api_token") String apiToken, @Query("limit") Integer limit, @Query("offset") Integer offset, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.PRODUCT_SEE)
    Call<Object> seeProduct(@Path("api_token") String apiToken, @Path("id") long feedId, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.PRODUCT_WANT)
    Call<Object> wantProduct(@Path("api_token") String apiToken, @Path("id") long feedId, @Query("want") boolean want, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @DELETE(RestConstants.PRODUCT_REMOVE)
    Call<Object> removeProduct(@Path("api_token") String apiToken, @Path("id") long feedId, @Query("signature") String signature);
}
