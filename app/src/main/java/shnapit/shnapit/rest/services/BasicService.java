package shnapit.shnapit.rest.services;

import retrofit2.Call;
import retrofit2.http.GET;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.responses.TimestampResponse;

public interface BasicService {

    @GET(RestConstants.TIMESTAMP)
    Call<TimestampResponse> getTimestamp();
}
