package shnapit.shnapit.rest.services;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.responses.ConnectionsResponse;
import shnapit.shnapit.rest.responses.SimpleMultipleUserResponse;

public interface ConnectionService {

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @GET(RestConstants.CONNECTIONS_GENERAL)
    Call<ConnectionsResponse> getConnections(@Path("api_token") String apiToken, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @GET(RestConstants.SEARCH_USERS)
    Call<SimpleMultipleUserResponse> searchForUsers(@Path("api_token") String apiToken, @Query("query_string") String filter, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @DELETE(RestConstants.DELETE_CONNECTION)
    Call<Object> deleteConnection(@Path("api_token") String apiToken, @Query("registration_type") String registrationType, @Query("friend") String friendUsername, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.CONNECTIONS_GENERAL)
    Call<Object> createConnection(@Path("api_token") String apiToken, @Query("registration_type") String registrationType, @Query("friends") String friendUsername, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.ACCEPT_REQUEST)
    Call<Object> acceptRequest(@Path("api_token") String apiToken, @Query("registration_type") String registrationType, @Query("friend") String friendUsername, @Query("signature") String signature);
}
