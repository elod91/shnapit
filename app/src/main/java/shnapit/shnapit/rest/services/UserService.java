package shnapit.shnapit.rest.services;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.request.LocationRequest;
import shnapit.shnapit.rest.request.UpdateCategoriesRequest;
import shnapit.shnapit.rest.responses.CategoriesResponse;
import shnapit.shnapit.rest.responses.ForgotPasswordResponse;
import shnapit.shnapit.rest.responses.SimpleUserResponse;

public interface UserService {

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.USER_LOGIN)
    Call<SimpleUserResponse> login(@Body User user);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.FACEBOOK_LOGIN)
    Call<User> loginWithFacebook(@Body User user);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.USER_REGISTER)
    Call<SimpleUserResponse> registerUser(@Body User user);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.USER_FORGOT_PASSWORD)
    Call<ForgotPasswordResponse> forgotPassword(@Body User user);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @GET(RestConstants.USER_VALIDATE_USERNAME)
    Call<Object> checkUsernameAvailability(@Query("user_name") String username, @Query("signature") String signature);


    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.USER_SEND_LOCATION)
    Call<Object> sendUserLocation(@Path("api_token") String apiToken, @Body LocationRequest locationRequest);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @GET(RestConstants.GET_CATEGORIES)
    Call<CategoriesResponse> getAllCategories(@Path("api_token") String apiToken, @Query("signature") String signature);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @POST(RestConstants.UPDATE_USER)
    Call<SimpleUserResponse> updateUserCategories(@Path("api_token") String apiToken, @Body UpdateCategoriesRequest updateCategoriesRequest);

    @Multipart
    @POST(RestConstants.UPDATE_USER)
    Call<SimpleUserResponse> uploadNewProfilePhoto(@Path("api_token") String apiToken, @Part("image_file\"; filename=\"profilePic.png") RequestBody image, @Part("signature") RequestBody signature, @Part("first_name") RequestBody firstName, @Part("last_name") RequestBody lastName, @Part("category_ids") RequestBody categoryIds);

    @Headers({"Content-Type : application/json", "Accept : application/json"})
    @DELETE(RestConstants.DELETE_USER)
    Call<Object> deleteUser(@Path("api_token") String apiToken, @Query("signature") String signature);


}
