package shnapit.shnapit.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lb.recyclerview_fast_scroller.RecyclerViewFastScroller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import shnapit.shnapit.R;
import shnapit.shnapit.adapters.ConnectionsAdapter;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.utils.UserComparator;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyConnectionsFragment extends Fragment {

    private RecyclerView friendsList;
    private RecyclerViewFastScroller fastScroller;
    private ConnectionsAdapter adapter;

    private List<User> connections;

    public MyConnectionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_connections, container, false);

        initViews(view);
        initRecyclerView(view);
        return view;
    }

    private void initViews(View view) {
        friendsList = (RecyclerView) view.findViewById(R.id.my_connections_friends_list);
        adapter = new ConnectionsAdapter(getActivity(), this);
        connections = new ArrayList<>();
    }

    private void initRecyclerView(View view) {
        fastScroller = (RecyclerViewFastScroller) view.findViewById(R.id.my_connections_fast_scroller);
        friendsList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                final int firstVisibleItemPosition = findFirstVisibleItemPosition();
                if (firstVisibleItemPosition != 0) {
                    // this avoids trying to handle un-needed calls
                    if (firstVisibleItemPosition == -1)
                        //not initialized, or no items shown, so hide fast-scroller
                        fastScroller.setVisibility(View.GONE);
                    return;
                }
                final int lastVisibleItemPosition = findLastVisibleItemPosition();
                int itemsShown = lastVisibleItemPosition - firstVisibleItemPosition + 1;
                //if all items are shown, hide the fast-scroller
                fastScroller.setVisibility(adapter.getItemCount() > itemsShown ? View.VISIBLE : View.GONE);
            }
        });

        fastScroller.setRecyclerView(friendsList);
        fastScroller.setViewsToUse(R.layout.fast_scroller, R.id.fast_scroller_bubble, R.id.fast_scroller_handle);
    }

    public void setRecyclerView(List<User> myConnections, ConnectionsAdapter.OnConnectionsChangedListener onConnectionsChangedListener) {
        connections = myConnections;

        adapter = new ConnectionsAdapter(getActivity(), this);
        adapter.setData(myConnections);
        adapter.setListener(onConnectionsChangedListener);

        friendsList.setAdapter(adapter);
    }

    public void changeToSearchedData(List<User> searchedData) {
        for (User searchedUser : searchedData) {
            searchedUser.setConnectionStatus(User.CONNECTION_STATUS_PENDING_REQUESTS);
            for (User connection : connections) {
                if (!connection.isAlphabet() && !searchedUser.isAlphabet() && searchedUser.getId().equals(connection.getId())) {
                    searchedUser.setConnectionStatus(connection.getConnectionStatus());
                }
            }
        }
        adapter.setData(searchedData);
    }

    public void resetConnections() {
        adapter.setData(connections);
    }

    public void setFilter(String filter) {
        if (connections == null || connections.isEmpty())
            return;

        if (filter == null) {
            adapter.setData(this.connections);
            return;
        }

        List<User> filteredConnections = new ArrayList<>();
        for (User connection : connections)
            if (!connection.isAlphabet() && (connection.getFirstName().toLowerCase().startsWith(filter.toLowerCase()) || connection.getLastName().toLowerCase().startsWith(filter.toLowerCase())))
                filteredConnections.add(connection);

        Collections.sort(filteredConnections, UserComparator.getInstance());

        addAlphabet(filteredConnections);
        adapter.setData(filteredConnections);
    }

    private void addAlphabet(List<User> connections) {
        if (connections == null || connections.isEmpty())
            return;

        Collections.sort(connections, UserComparator.getInstance());
        String previousLetter = null;
        for (int i = 0; i < connections.size(); i++) {
            if (previousLetter == null || !previousLetter.equalsIgnoreCase(connections.get(i).getFirstName().substring(0, 1))) {
                previousLetter = connections.get(i).getFirstName().substring(0, 1).toUpperCase();

                connections.add(i, new User(true, previousLetter));
            }
        }
    }
}
