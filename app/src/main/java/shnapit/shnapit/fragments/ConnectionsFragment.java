package shnapit.shnapit.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.adapters.ConnectionsAdapter;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.UserDbManager;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.fragments.base.BaseFragment;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.responses.ConnectionsResponse;
import shnapit.shnapit.rest.responses.SimpleMultipleUserResponse;
import shnapit.shnapit.rest.services.ConnectionService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.GeneralUtil;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.ProgressFactory;
import shnapit.shnapit.utils.TagFormat;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConnectionsFragment extends BaseFragment implements View.OnClickListener, TextView.OnEditorActionListener, ConnectionsAdapter.OnConnectionsChangedListener {
    public static final int NUMBER_OF_PAGES = 2;

    private RelativeLayout searchContainer;
    private ViewPager viewPager;
    private EditText searchInput;
    private TextView myConnections, requests, requestsCounter, cancelSearch;

    private Fragment myConnectionsFragment, requestsFragment;

    private ConnectionService connectionService;
    private String apiToken;

    private boolean serverSearch;

    private Comparator<User> userComparator = new Comparator<User>() {
        @Override
        public int compare(User lhs, User rhs) {
            return lhs.getFirstName().toLowerCase().compareTo(rhs.getFirstName().toLowerCase());
        }
    };

    public ConnectionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connections, container, false);

        initViews(view);
        setOnClickListeners(myConnections, requests, searchInput, cancelSearch);
        setViewPager();
        setTextWatcher();
        getMyConnections();
        return view;
    }

    @Override
    public int getToolbarTitle() {
        return R.string.drawer_connections;
    }

    @Override
    public boolean showLeftAction() {
        return true;
    }

    @Override
    public int getLeftActionRes() {
        return R.drawable.ic_toggle;
    }

    @Override
    public void leftAction() {
        navigationListener.toggleDrawer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.connections_my_connections:
                switchToMyConnections();
                break;

            case R.id.connections_requests:
                switchToRequests();
                break;

            case R.id.connections_search_input:
                cancelSearch.setVisibility(View.VISIBLE);
                break;

            case R.id.connections_search_cancel:
                cancelSearch();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
            GeneralUtil.hideSoftKeyboard(getActivity());
            serverSearch = true;

            ProgressFactory.showProgressDialog(getActivity());
            connectionService.searchForUsers(apiToken, searchInput.getText().toString(), RestClient.getSignature(getActivity(), apiToken, TagFormat.from(RestConstants.SEARCH_USERS).with("api_token", apiToken).format())).enqueue(new Callback<SimpleMultipleUserResponse>() {
                @Override
                public void onResponse(Response<SimpleMultipleUserResponse> response) {
                    if (response.body() == null) {
                        ProgressFactory.hideProgressDialog();
                        RestClient.handleErrors(getActivity(), response.errorBody());
                        return;
                    }

                    Collections.sort(response.body().getUsers(), userComparator);

                    addAlphabet(response.body().getUsers());
                    if (myConnectionsFragment instanceof MyConnectionsFragment)
                        ((MyConnectionsFragment) myConnectionsFragment).changeToSearchedData(response.body().getUsers());
                    ProgressFactory.hideProgressDialog();
                }

                @Override
                public void onFailure(Throwable t) {
                    ProgressFactory.hideProgressDialog();
                    DialogFactory.showDialogOneButton(getActivity(), getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
                }
            });
            searchInput.setText(null);
        }
        return false;
    }

    @Override
    public void onConnectionChanged() {
        getMyConnections();
        cancelSearch();

        navigationListener.connectionsChanged();
    }

    private void initViews(View view) {
        searchContainer = (RelativeLayout) view.findViewById(R.id.connections_search_container);

        viewPager = (ViewPager) view.findViewById(R.id.connections_pager);
        searchInput = (EditText) view.findViewById(R.id.connections_search_input);

        myConnections = (TextView) view.findViewById(R.id.connections_my_connections);
        requests = (TextView) view.findViewById(R.id.connections_requests);
        requestsCounter = (TextView) view.findViewById(R.id.connections_requests_counter);
        cancelSearch = (TextView) view.findViewById(R.id.connections_search_cancel);
    }

    private void setViewPager() {
        myConnectionsFragment = new MyConnectionsFragment();
        requestsFragment = new ConnectionRequestsFragment();

        PagerAdapter pagerAdapter = new ConnectionsPagerAdapter(getFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case ConnectionsPagerAdapter.MY_CONNECTIONS:
                        switchToMyConnections();
                        break;

                    case ConnectionsPagerAdapter.REQUESTS:
                        switchToRequests();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        switchToMyConnections();
    }

    private void setTextWatcher() {
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (serverSearch) {
                    serverSearch = false;
                    return;
                }

                if (s.toString().isEmpty()) {
                    cancelSearch.setVisibility(View.GONE);
                    if (myConnectionsFragment instanceof MyConnectionsFragment)
                        ((MyConnectionsFragment) myConnectionsFragment).setFilter(null);
                    return;
                }

                cancelSearch.setVisibility(View.VISIBLE);
                if (myConnectionsFragment instanceof MyConnectionsFragment)
                    ((MyConnectionsFragment) myConnectionsFragment).setFilter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getMyConnections() {
        if (!RestClient.hasNetworkConnection(getActivity())) {
            long loggedInUserId = PersistentUtils.getLong(getActivity(), PersistentUtils.LOGGED_IN_USER_ID, -1);
            if (loggedInUserId == -1)
                return;

            UserDbManager.getInstance().selectAllUsersExcludingOne(loggedInUserId, new ShnapitDatabase.OnResultReceivedListener<List<User>>() {
                @Override
                public void onResultReceived(List<User> result) {
                    List<User> myConnections = new ArrayList<>();
                    List<User> requests = new ArrayList<>();

                    for (User user : result)
                        if (user.getConnectionStatus() == User.CONNECTION_STATUS_PENDING_REQUESTS)
                            requests.add(user);
                        else myConnections.add(user);

                    Collections.sort(myConnections, userComparator);
                    Collections.sort(requests, userComparator);

                    requestsCounter.setVisibility(View.GONE);
                    if (!requests.isEmpty()) {
                        requestsCounter.setVisibility(View.VISIBLE);
                        requestsCounter.setText(String.format("%d", requests.size()));
                    }

                    addAlphabet(myConnections);
                    addAlphabet(requests);
                    if (myConnectionsFragment instanceof MyConnectionsFragment)
                        ((MyConnectionsFragment) myConnectionsFragment).setRecyclerView(myConnections, ConnectionsFragment.this);

                    if (requestsFragment instanceof ConnectionRequestsFragment)
                        ((ConnectionRequestsFragment) requestsFragment).setRecyclerView(requests, ConnectionsFragment.this);
                }
            });
            return;
        }

        if (apiToken == null)
            apiToken = PersistentUtils.getString(getActivity(), PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);

        if (connectionService == null)
            connectionService = RestClient.getInstance().getRetrofit().create(ConnectionService.class);

        connectionService.getConnections(apiToken, RestClient.getSignature(getActivity(), apiToken, TagFormat.from(RestConstants.CONNECTIONS_GENERAL).with("api_token", apiToken).format())).enqueue(new Callback<ConnectionsResponse>() {
            @Override
            public void onResponse(Response<ConnectionsResponse> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(getActivity(), response.errorBody());
                    return;
                }

                saveMyConnections(response.body());
            }

            @Override
            public void onFailure(Throwable t) {
                DialogFactory.showDialogOneButton(getActivity(), getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void saveMyConnections(ConnectionsResponse response) {
        List<User> connectionsToSave = new ArrayList<>();
        for (User acceptedConnection : response.getAcceptedConnections())
            acceptedConnection.setConnectionStatus(User.CONNECTION_STATUS_ACCEPTED);

        for (User pendingConnection : response.getPendingConnections())
            pendingConnection.setConnectionStatus(User.CONNECTION_STATUS_PENDING);

        for (User requestConnection : response.getPendingConnectionRequests())
            requestConnection.setConnectionStatus(User.CONNECTION_STATUS_PENDING_REQUESTS);

        connectionsToSave.addAll(response.getAcceptedConnections());
        connectionsToSave.addAll(response.getPendingConnections());
        connectionsToSave.addAll(response.getPendingConnectionRequests());

        if (connectionsToSave.isEmpty()) {
            if (myConnectionsFragment instanceof MyConnectionsFragment)
                ((MyConnectionsFragment) myConnectionsFragment).setRecyclerView(new ArrayList<User>(), ConnectionsFragment.this);

            if (requestsFragment instanceof ConnectionRequestsFragment)
                ((ConnectionRequestsFragment) requestsFragment).setRecyclerView(new ArrayList<User>(), ConnectionsFragment.this);
            return;
        }

        UserDbManager.getInstance().insertMultipleUsers(connectionsToSave.toArray(new User[connectionsToSave.size()]), new ShnapitDatabase.OnResultReceivedListener<List<User>>() {
            @Override
            public void onResultReceived(List<User> result) {
                List<User> myConnections = new ArrayList<>();
                List<User> requests = new ArrayList<>();

                for (User user : result)
                    if (user.getConnectionStatus() == User.CONNECTION_STATUS_PENDING_REQUESTS)
                        requests.add(user);
                    else myConnections.add(user);

                Collections.sort(myConnections, userComparator);
                Collections.sort(requests, userComparator);

                requestsCounter.setVisibility(View.GONE);
                if (!requests.isEmpty()) {
                    requestsCounter.setVisibility(View.VISIBLE);
                    requestsCounter.setText(String.format("%d", requests.size()));
                }

                addAlphabet(myConnections);
                addAlphabet(requests);
                if (myConnectionsFragment instanceof MyConnectionsFragment)
                    ((MyConnectionsFragment) myConnectionsFragment).setRecyclerView(myConnections, ConnectionsFragment.this);

                if (requestsFragment instanceof ConnectionRequestsFragment)
                    ((ConnectionRequestsFragment) requestsFragment).setRecyclerView(requests, ConnectionsFragment.this);
            }
        });
    }

    private void switchToMyConnections() {
        searchContainer.setAlpha(1);

        myConnections.setSelected(true);
        requests.setSelected(false);

        searchInput.setEnabled(true);
        searchInput.setOnEditorActionListener(this);

        viewPager.setCurrentItem(ConnectionsPagerAdapter.MY_CONNECTIONS, true);
    }

    private void switchToRequests() {
        searchContainer.setAlpha(0.25f);

        requests.setSelected(true);
        myConnections.setSelected(false);

        searchInput.setEnabled(false);
        searchInput.setText(null);
        searchInput.setOnEditorActionListener(null);
        cancelSearch.setVisibility(View.GONE);

        viewPager.setCurrentItem(ConnectionsPagerAdapter.REQUESTS, true);
    }

    private void cancelSearch() {
        GeneralUtil.hideSoftKeyboard(getActivity());

        cancelSearch.setVisibility(View.GONE);
        searchInput.setText(null);
    }

    private void addAlphabet(List<User> connections) {
        if (connections == null || connections.isEmpty())
            return;

        String previousLetter = null;
        for (int i = 0; i < connections.size(); i++) {
            if (previousLetter == null || !previousLetter.equalsIgnoreCase(connections.get(i).getFirstName().substring(0, 1))) {
                previousLetter = connections.get(i).getFirstName().substring(0, 1).toUpperCase();

                connections.add(i, new User(true, previousLetter));
            }
        }
    }

    private class ConnectionsPagerAdapter extends FragmentStatePagerAdapter {
        public static final int MY_CONNECTIONS = 0;
        public static final int REQUESTS = 1;

        public ConnectionsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment returnFragment = null;

            switch (position) {
                case MY_CONNECTIONS:
                    myConnectionsFragment = new MyConnectionsFragment();
                    returnFragment = myConnectionsFragment;
                    break;

                case REQUESTS:
                    requestsFragment = new ConnectionRequestsFragment();
                    returnFragment = requestsFragment;
            }
            return returnFragment;
        }

        @Override
        public int getCount() {
            return NUMBER_OF_PAGES;
        }
    }
}
