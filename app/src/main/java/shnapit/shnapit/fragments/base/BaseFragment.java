package shnapit.shnapit.fragments.base;

import android.support.v4.app.Fragment;
import android.view.View;

import shnapit.shnapit.fragments.FragmentNavigationListener;
import shnapit.shnapit.fragments.FragmentToolbarInterface;

public abstract class BaseFragment extends Fragment implements FragmentToolbarInterface, View.OnClickListener {

    protected FragmentNavigationListener navigationListener;

    @Override
    public void onResume() {
        super.onResume();

        if (navigationListener != null)
            navigationListener.closeDrawer();
    }

    @Override
    public boolean showRightAction() {
        return false;
    }

    @Override
    public int getRightActionRes() {
        return 0;
    }

    @Override
    public boolean showLeftAction() {
        return false;
    }

    @Override
    public int getLeftActionRes() {
        return 0;
    }

    @Override
    public void leftAction() {

    }

    @Override
    public void rightAction() {

    }

    @Override
    public void titleClicked() {

    }

    @Override
    public void onClick(View v) {

    }

    public void setNavigationListener(FragmentNavigationListener fragmentNavigationListener) {
        navigationListener = fragmentNavigationListener;
    }

    protected void setOnClickListeners(View... views) {
        for (View view : views)
            view.setOnClickListener(this);
    }
}
