package shnapit.shnapit.fragments;

public interface FragmentToolbarInterface {

    int getToolbarTitle();

    boolean showRightAction();

    int getRightActionRes();

    boolean showLeftAction();

    int getLeftActionRes();

    void leftAction();

    void rightAction();

    void titleClicked();
}
