package shnapit.shnapit.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.activities.HomeActivity;
import shnapit.shnapit.activities.ProductWebsiteActivity;
import shnapit.shnapit.database.models.Product;
import shnapit.shnapit.fragments.base.BaseFragment;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.responses.ProductListResponse;
import shnapit.shnapit.rest.services.ProductService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.PictureSwipeOnTouchListener;
import shnapit.shnapit.utils.TagFormat;
import shnapit.shnapit.views.SquareImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscoveryFragment extends BaseFragment implements Animation.AnimationListener, PictureSwipeOnTouchListener.OnPictureSwipeListener {
    public static final int PRODUCT_LIMIT = 20;

    private RelativeLayout mainContainer, imageContainer, loseIt, buyIt, saveIt;
    private ImageView foregroundImage, backgroundImage, authorImage;
    private TextView brandAndName, price, category, seenByCounter, authorName, wantedCounter, noData;

    private Animation slideRight, slideLeft;
    private PictureSwipeOnTouchListener pictureSwipeOnTouchListener;

    private List<Product> discoveryProducts;

    private ProductService productService;
    private String apiToken;

    private boolean animating, gettingProducts, firstRun, savedIt;
    private int offset;

    public DiscoveryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_discovery, container, false);
        initViews(view);
        setOnClickListeners(loseIt, buyIt, saveIt);
        initAnimations();
        initPictureSwipeListener();
        initVariables();
        getProducts();

        return view;
    }

    @Override
    public int getToolbarTitle() {
        return R.string.drawer_discover;
    }

    @Override
    public boolean showRightAction() {
        return true;
    }

    @Override
    public int getRightActionRes() {
        return R.drawable.ic_saved;
    }

    @Override
    public boolean showLeftAction() {
        return true;
    }

    @Override
    public int getLeftActionRes() {
        return R.drawable.ic_toggle;
    }

    @Override
    public void leftAction() {
        if (navigationListener != null)
            navigationListener.toggleDrawer();
    }

    @Override
    public void rightAction() {
        if (navigationListener != null)
            navigationListener.setSelected(HomeActivity.DRAWER_IDENTIFIER_SAVED);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.discovery_lose_it:
                loseIt();
                break;

            case R.id.discovery_buy_it:
                buyIt();
                break;

            case R.id.discovery_save_it:
                saveIt();
                break;

            default:
                super.onClick(v);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {
        animating = true;
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        animating = false;

        if (savedIt)
            DialogFactory.showSwipeActionDialog(getActivity(), "Saved it!", R.drawable.ic_big_save);
        else DialogFactory.showSwipeActionDialog(getActivity(), "Lost it!", R.drawable.ic_big_lose);

        showNextProduct();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onLike() {
        saveIt();
    }

    @Override
    public void onDislike() {
        loseIt();
    }

    private void initViews(View root) {
        mainContainer = (RelativeLayout) root.findViewById(R.id.discovery_main_container);
        imageContainer = (RelativeLayout) root.findViewById(R.id.discovery_image_container);
        loseIt = (RelativeLayout) root.findViewById(R.id.discovery_lose_it);
        buyIt = (RelativeLayout) root.findViewById(R.id.discovery_buy_it);
        saveIt = (RelativeLayout) root.findViewById(R.id.discovery_save_it);

        foregroundImage = (ImageView) root.findViewById(R.id.discovery_foreground_image);
        backgroundImage = (ImageView) root.findViewById(R.id.discovery_background_image);
        authorImage = (ImageView) root.findViewById(R.id.author_image);

        brandAndName = (TextView) root.findViewById(R.id.brand_and_name);
        price = (TextView) root.findViewById(R.id.price);
        category = (TextView) root.findViewById(R.id.category);
        seenByCounter = (TextView) root.findViewById(R.id.seen_by_counter);
        authorName = (TextView) root.findViewById(R.id.author_name);
        wantedCounter = (TextView) root.findViewById(R.id.wanted_by_counter);
        noData = (TextView) root.findViewById(R.id.discovery_no_data);
    }

    private void initAnimations() {
        slideRight = AnimationUtils.loadAnimation(getActivity(), R.anim.out_to_right);
        slideRight.setAnimationListener(this);

        slideLeft = AnimationUtils.loadAnimation(getActivity(), R.anim.out_to_left);
        slideLeft.setAnimationListener(this);
    }

    private void initPictureSwipeListener() {
        pictureSwipeOnTouchListener = new PictureSwipeOnTouchListener(getActivity().getResources().getDisplayMetrics().density);
        pictureSwipeOnTouchListener.setOnPictureSwipeListener(this);
    }

    private void initVariables() {
        firstRun = true;
        offset = -PRODUCT_LIMIT;
    }

    private void getProducts() {
        if (apiToken == null)
            apiToken = PersistentUtils.getString(getActivity(), PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);

        if (productService == null)
            productService = RestClient.getInstance().getRetrofit().create(ProductService.class);

        gettingProducts = true;
        offset += PRODUCT_LIMIT;
        productService.getProductsForDiscovery(apiToken, PRODUCT_LIMIT, offset, RestClient.getSignature(getActivity(), apiToken, TagFormat.from(RestConstants.PRODUCT_GENERAL + ".json").with("api_token", apiToken).format())).enqueue(new Callback<ProductListResponse>() {
            @Override
            public void onResponse(Response<ProductListResponse> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(getActivity(), response.errorBody());
                    return;
                }

                handleData(response.body().getProducts());
            }

            @Override
            public void onFailure(Throwable t) {
                DialogFactory.showDialogOneButton(getActivity(), getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void handleData(List<Product> response) {
        if (response.size() < PRODUCT_LIMIT)
            offset = -PRODUCT_LIMIT;

        if (response.isEmpty()) {
            getProducts();
            return;
        }

        if (discoveryProducts == null)
            discoveryProducts = new ArrayList<>();
        discoveryProducts.addAll(response);

        gettingProducts = false;
        if (firstRun) {
            if (response.isEmpty()) {
                mainContainer.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
                return;
            }

            if (response.size() == 1) {
                setProduct(response.get(0), null);
                return;
            }

            setProduct(response.get(0), response.get(1));
        }
    }

    private void setProduct(final Product foregroundProduct, final Product backgroundProduct) {
        buyIt.setVisibility(View.VISIBLE);
        buyIt.setOnClickListener(this);
        if (foregroundProduct.getWebsite() == null || foregroundProduct.getWebsite().isEmpty()) {
            buyIt.setVisibility(View.INVISIBLE);
            buyIt.setOnClickListener(null);
        }
        seeProduct(foregroundProduct);

        if (firstRun) {
            Picasso.with(getActivity()).load(foregroundProduct.getPhotoUrl()).into(foregroundImage, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    setForegroundProductInfo(foregroundProduct);
                    foregroundImage.setOnTouchListener(pictureSwipeOnTouchListener);
                    if (backgroundProduct != null)
                        Picasso.with(getActivity()).load(backgroundProduct.getPhotoUrl()).into(backgroundImage);
                }

                @Override
                public void onError() {

                }
            });
            firstRun = false;
            return;
        }

        setForegroundProductInfo(foregroundProduct);
        imageContainer.removeViewAt(1);

        foregroundImage = backgroundImage;
        foregroundImage.setOnTouchListener(pictureSwipeOnTouchListener);

        backgroundImage = new SquareImageView(getActivity());
        backgroundImage.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        backgroundImage.setBackgroundResource(R.color.colorPrimary);

        imageContainer.addView(backgroundImage, 0);

        if (backgroundProduct != null)
            Picasso.with(getActivity()).load(backgroundProduct.getPhotoUrl()).into(backgroundImage);
    }

    private void setForegroundProductInfo(Product product) {
        brandAndName.setText(String.format("%s - %s", product.getBrand(), product.getName()));
        price.setText(product.getPrice());
        category.setText(product.getCategory().get(0).getName());
        seenByCounter.setText(String.format("%d", product.getSeenByCounter()));
        wantedCounter.setText(String.format("%d", product.getWantedByCount()));
        if (product.getUpdatedBy() != null) {
            authorImage.setImageResource(R.drawable.ic_user_default);
            if (product.getUpdatedBy().getProfileImage() != null && !product.getUpdatedBy().getProfileImage().isEmpty())
                Picasso.with(getActivity()).load(product.getUpdatedBy().getProfileImage()).into(authorImage);
            authorName.setText(String.format("%s %s", product.getUpdatedBy().getFirstName(), product.getUpdatedBy().getLastName()));
            return;
        }

        authorName.setText(R.string.product_default_author);
        authorImage.setImageResource(R.drawable.ic_discovery_toolbar);
    }

    private void showNextProduct() {
        discoveryProducts.remove(0);

        if (!gettingProducts && discoveryProducts.size() < 10)
            getProducts();

        setProduct(discoveryProducts.get(0), discoveryProducts.get(1));
    }

    private void loseIt() {
        if (!wantProduct(false))
            return;

        savedIt = false;
        foregroundImage.startAnimation(slideLeft);
    }

    private void buyIt() {
        if (!RestClient.hasNetworkConnection(getActivity(), true))
            return;

        Intent intent = new Intent(getActivity(), ProductWebsiteActivity.class);
        intent.putExtra(ProductWebsiteActivity.PRODUCT_WEBSITE, discoveryProducts.get(0).getWebsite());
        intent.putExtra(ProductWebsiteActivity.PRODUCT_NAME, discoveryProducts.get(0).getName());

        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);
    }

    private void saveIt() {
        if (!wantProduct(true))
            return;

        savedIt = true;
        foregroundImage.startAnimation(slideRight);
    }

    private boolean wantProduct(boolean want) {
        if (animating || !RestClient.hasNetworkConnection(getActivity(), true))
            return false;

        productService.wantProduct(apiToken, discoveryProducts.get(0).getId(), want, RestClient.getSignature(getActivity(), apiToken, TagFormat.from(RestConstants.PRODUCT_WANT).with("api_token", apiToken).with("id", discoveryProducts.get(0).getId()).format())).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Response<Object> response) {
                //DO NOTHING
            }

            @Override
            public void onFailure(Throwable t) {
                //DO NOTHING
            }
        });
        return true;
    }

    private void seeProduct(Product product) {
        productService.seeProduct(apiToken, product.getId(), RestClient.getSignature(getActivity(), apiToken, TagFormat.from(RestConstants.PRODUCT_SEE).with("api_token", apiToken).with("id", product.getId()).format())).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Response<Object> response) {
                //DO NOTHING
            }

            @Override
            public void onFailure(Throwable t) {
                //DO NOTHING
            }
        });
    }
}
