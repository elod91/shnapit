package shnapit.shnapit.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.activities.EditProductActivity;
import shnapit.shnapit.activities.HomeActivity;
import shnapit.shnapit.activities.WebPictureActivity;
import shnapit.shnapit.adapters.ProductAdapter;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.ProductDbManager;
import shnapit.shnapit.database.models.Product;
import shnapit.shnapit.fragments.base.BaseFragment;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.responses.ProductListResponse;
import shnapit.shnapit.rest.services.ProductService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.EndlessRecyclerOnScrollListener;
import shnapit.shnapit.utils.GeneralUtil;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.SavedListRecyclerView;
import shnapit.shnapit.utils.TagFormat;
import shnapit.shnapit.utils.ViewUtils;

public class SavedFragment extends BaseFragment implements ImageChooserListener, SavedListRecyclerView.HideShowSearchBarListener, Animation.AnimationListener, TextView.OnEditorActionListener, ProductAdapter.OnProductRefreshListener {
    public static final int REQUEST_PERMISSION_FROM_CAMERA = 0x1;
    public static final int REQUEST_PERMISSION_FROM_GALLERY = 0x2;
    public static final int PRODUCT_LIMIT = 8;

    private SwipeRefreshLayout swipeToRefresh;
    private SavedListRecyclerView savedList;
    private RelativeLayout searchContainer, newPosts;
    private EditText searchInput;

    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    private ProductAdapter adapter;

    private ProductService productService;

    private Animation inFromTop, outToTop;

    private ImageChooserManager imageChooserManager;
    private String filePath, apiToken;

    private int chooserType, offset, needToRefresh;
    private boolean animationRunning;

    public SavedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saved, container, false);
        initViews(view);
        initSaveList();
        getProducts();
        setOnClickListeners(newPosts, view.findViewById(R.id.saved_search_cancel), view.findViewById(R.id.saved_post_photo), view.findViewById(R.id.saved_take_photo), view.findViewById(R.id.saved_share_web_photo));

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("media_path"))
                filePath = savedInstanceState.getString("media_path");

            if (savedInstanceState.containsKey("chooser_type"))
                chooserType = savedInstanceState.getInt("chooser_type");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (needToRefresh == 1)
            refreshData();

        needToRefresh--;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (chooserType != 0)
            outState.putInt("chooser_type", chooserType);

        if (filePath != null)
            outState.putString("media_path", filePath);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_FROM_CAMERA || requestCode == REQUEST_PERMISSION_FROM_GALLERY) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestCode == REQUEST_PERMISSION_FROM_CAMERA) {
                    takePhoto();
                    return;
                }

                chooseImageFromGallery();
            } else {
                DialogFactory.showDialogOneButton(getActivity(), getString(R.string.dialog_error_title), getString(R.string.dialog_no_write_permission_message), new DialogFactory.OnButtonPressedListener() {
                    @Override
                    public void onButtonPressed() {
                        if (requestCode == REQUEST_PERMISSION_FROM_CAMERA) {
                            takePhoto();
                            return;
                        }

                        chooseImageFromGallery();
                    }
                });
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE) {
            if (imageChooserManager == null)
                reinitializeImageChooser();

            imageChooserManager.submit(requestCode, data);
            return;
        }

        if (requestCode == Crop.REQUEST_CROP) {
            Intent intent = new Intent(getActivity(), EditProductActivity.class);
            intent.putExtra(EditProductActivity.IMAGE_PATH, filePath);
            intent.putExtra(EditProductActivity.OPERATION, EditProductActivity.OPERATION_CREATE);

            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);

            needToRefresh = 2;
        }
    }

    @Override
    public int getToolbarTitle() {
        return R.string.drawer_saved;
    }

    @Override
    public boolean showRightAction() {
        return true;
    }

    @Override
    public int getRightActionRes() {
        return R.drawable.ic_categories_toolbar;
    }

    @Override
    public boolean showLeftAction() {
        return true;
    }

    @Override
    public int getLeftActionRes() {
        return R.drawable.ic_discovery_toolbar;
    }

    @Override
    public void leftAction() {
        navigationListener.setSelected(HomeActivity.DRAWER_IDENTIFIER_DISCOVERY);
    }

    @Override
    public void rightAction() {
        navigationListener.setCategories();
    }

    @Override
    public void titleClicked() {
        refreshData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saved_new_posts:
                ViewUtils.collapse(newPosts);
                refreshData();
                break;

            case R.id.saved_search_cancel:
                refreshData();
                break;

            case R.id.saved_post_photo:
                chooseImageFromGallery();
                break;

            case R.id.saved_take_photo:
                takePhoto();
                break;

            case R.id.saved_share_web_photo:
                needToRefresh = 1;
                startActivity(new Intent(getActivity(), WebPictureActivity.class));
                getActivity().overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);
        }
    }

    @Override
    public void onImageChosen(ChosenImage chosenImage) {
        filePath = chosenImage.getFilePathOriginal();

        Crop.of(Uri.fromFile(new File(filePath)), Uri.fromFile(new File(filePath))).asSquare().start(getActivity(), this);
    }

    @Override
    public void onError(final String reason) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogFactory.showDialogOneButton(getActivity(), getString(R.string.dialog_error_title), reason, null);
            }
        });
    }

    @Override
    public void showSearchBar() {
        if (animationRunning || searchContainer.getVisibility() == View.VISIBLE)
            return;

        if (inFromTop == null) {
            inFromTop = AnimationUtils.loadAnimation(getActivity(), R.anim.in_from_top_search);
            inFromTop.setAnimationListener(this);
        }

        searchContainer.startAnimation(inFromTop);
    }

    @Override
    public void hideSearchBar() {
        if (animationRunning || searchContainer.getVisibility() == View.GONE)
            return;

        if (outToTop == null) {
            outToTop = AnimationUtils.loadAnimation(getActivity(), R.anim.out_to_top_search);
            outToTop.setAnimationListener(this);
        }

        searchContainer.startAnimation(outToTop);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        animationRunning = true;
        if (animation.equals(inFromTop))
            searchContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        animationRunning = false;
        if (animation.equals(outToTop))
            searchContainer.setVisibility(View.GONE);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
            GeneralUtil.hideSoftKeyboard(getActivity());

            adapter.removeData();
            offset = 0;
            getProducts();
        }
        return false;
    }

    @Override
    public void doRefresh() {
        needToRefresh = 1;
    }

    private void initViews(View view) {
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.saved_swipe_to_refresh);
        savedList = (SavedListRecyclerView) view.findViewById(R.id.saved_list);
        searchContainer = (RelativeLayout) view.findViewById(R.id.saved_search_container);
        newPosts = (RelativeLayout) view.findViewById(R.id.saved_new_posts);
        searchInput = (EditText) view.findViewById(R.id.saved_search_input);
        searchInput.setOnEditorActionListener(this);
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty())
                    searchContainer.findViewById(R.id.saved_search_cancel).setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initSaveList() {
        swipeToRefresh.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.loginBlue));
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                offset = currentPage * PRODUCT_LIMIT;
                getProducts();
            }

            @Override
            public void setSwipeToRefreshEnabled(boolean enabled) {
                swipeToRefresh.setEnabled(enabled);
            }
        };

        adapter = new ProductAdapter(getActivity(), this);
//        adapter.setShowDropdown(true);
        adapter.setShowDropdown(PersistentUtils.getBoolean(getActivity(), PersistentUtils.SHOW_DROPDOWN, true));

        savedList.setHasFixedSize(true);
        savedList.setLayoutManager(layoutManager);
        savedList.setAdapter(adapter);
        savedList.addOnScrollListener(endlessRecyclerOnScrollListener);
        savedList.setDisplayHeight(ViewUtils.getScreenHeight(getActivity()));
        savedList.setHideShowSearchBarListener(this);
    }

    public void refreshData() {
        offset = 0;
        searchInput.setText(null);
        searchContainer.findViewById(R.id.saved_search_cancel).setVisibility(View.GONE);
        adapter.removeData();
        getProducts();
    }

    public void showNewPosts() {
        ViewUtils.expand(newPosts);
    }

    private void getProducts() {
        if (!RestClient.hasNetworkConnection(getActivity())) {
            ProductDbManager.getInstance().selectAllProducts(new ShnapitDatabase.OnResultReceivedListener<List<Product>>() {
                @Override
                public void onResultReceived(List<Product> result) {
                    adapter.setData(result);
                }
            });

            savedList.removeOnScrollListener(endlessRecyclerOnScrollListener);
            swipeToRefresh.setEnabled(false);
            return;
        }

        if (apiToken == null)
            apiToken = PersistentUtils.getString(getActivity(), PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);

        if (productService == null)
            productService = RestClient.getInstance().getRetrofit().create(ProductService.class);

        productService.getProductsForSaved(apiToken, PRODUCT_LIMIT, offset, null, searchInput.getText().toString().isEmpty() ? null : searchInput.getText().toString(), RestClient.getSignature(getActivity(), apiToken, TagFormat.from(RestConstants.PRODUCT_TIMELINE).with("api_token", apiToken).format())).enqueue(new Callback<ProductListResponse>() {
            @Override
            public void onResponse(Response<ProductListResponse> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(getActivity(), response.errorBody());
                    return;
                }

                saveProducts(response.body().getProducts());
            }

            @Override
            public void onFailure(Throwable t) {
                DialogFactory.showDialogOneButton(getActivity(), getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void chooseImageFromGallery() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_FROM_GALLERY);
            return;
        }

        chooserType = ChooserType.REQUEST_PICK_PICTURE;

        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();

        try {
            filePath = imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }
    }

    private void takePhoto() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_FROM_CAMERA);
            return;
        }

        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;

        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);

        try {
            filePath = imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }
    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    private void saveProducts(List<Product> products) {
        for (Product product : products) {
            Picasso.with(getActivity()).load(product.getPhotoUrl()).fetch();

            if (product.getCategory() != null && !product.getCategory().isEmpty()) {
                product.setCategoryName(product.getCategory().get(0).getName());
                product.setCategoryId(product.getCategory().get(0).getId());
            }

            if (product.getKeywords() != null)
                product.setKeywordsJoint(TextUtils.join(",", product.getKeywords()));

            if (product.getShareWith() != null && product.getShareWith().getId() != null)
                product.setShareWithId(product.getShareWith().getId());
        }

        if (products.isEmpty()) {
            if (swipeToRefresh.isRefreshing())
                swipeToRefresh.setRefreshing(false);
            return;
        }

        ProductDbManager.getInstance().insertMultipleProducts(products.toArray(new Product[products.size()]), new ShnapitDatabase.OnResultReceivedListener<List<Product>>() {
            @Override
            public void onResultReceived(List<Product> result) {
                adapter.addMultipleData(result);
                if (offset == 0) {
                    savedList.scrollToPosition(0);
                    savedList.scrollBy(0, (int) getResources().getDimension(R.dimen.saved_search_bar_height));
                    searchContainer.setVisibility(View.GONE);
                }

                if (swipeToRefresh.isRefreshing())
                    swipeToRefresh.setRefreshing(false);
            }
        });
    }
}
