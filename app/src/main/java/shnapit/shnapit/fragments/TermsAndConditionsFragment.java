package shnapit.shnapit.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import shnapit.shnapit.R;
import shnapit.shnapit.fragments.base.BaseFragment;

public class TermsAndConditionsFragment extends BaseFragment {
    public static final String TERMS_AND_CONDITIONS_URL = "file:///android_asset/shnapit_terms_conditions.html";

    public TermsAndConditionsFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
        loadTermsAndConditions(view);

        return view;
    }

    @Override
    public int getToolbarTitle() {
        return R.string.terms_and_conditions_title;
    }

    @Override
    public boolean showLeftAction() {
        return true;
    }

    @Override
    public int getLeftActionRes() {
        return R.drawable.ic_toggle;
    }

    @Override
    public void leftAction() {
        navigationListener.toggleDrawer();
    }

    private void loadTermsAndConditions(View view) {
        WebView webView = (WebView) view.findViewById(R.id.terms_and_conditions_container);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(TERMS_AND_CONDITIONS_URL);
    }
}
