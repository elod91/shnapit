package shnapit.shnapit.fragments;

import shnapit.shnapit.fragments.base.BaseFragment;

public interface FragmentNavigationListener {

    <B extends BaseFragment> void navigateTo(Class<B> fragmentClass);

    void setSelected(int identifier);

    void setCategories();

    void toggleDrawer();

    void closeDrawer();

    void connectionsChanged();
}
