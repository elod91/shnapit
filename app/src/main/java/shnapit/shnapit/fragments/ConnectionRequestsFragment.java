package shnapit.shnapit.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lb.recyclerview_fast_scroller.RecyclerViewFastScroller;

import java.util.List;

import shnapit.shnapit.R;
import shnapit.shnapit.adapters.ConnectionsAdapter;
import shnapit.shnapit.database.models.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConnectionRequestsFragment extends Fragment {

    private RecyclerView requestsList;
    private RecyclerViewFastScroller fastScroller;
    private ConnectionsAdapter adapter;

    public ConnectionRequestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_connections, container, false);

        initViews(view);
        initRecyclerView(view);
        return view;
    }

    private void initViews(View view) {
        requestsList = (RecyclerView) view.findViewById(R.id.my_connections_friends_list);
    }

    private void initRecyclerView(View view) {
        fastScroller = (RecyclerViewFastScroller) view.findViewById(R.id.my_connections_fast_scroller);
        requestsList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                final int firstVisibleItemPosition = findFirstVisibleItemPosition();
                if (firstVisibleItemPosition != 0) {
                    // this avoids trying to handle un-needed calls
                    if (firstVisibleItemPosition == -1)
                        //not initialized, or no items shown, so hide fast-scroller
                        fastScroller.setVisibility(View.GONE);
                    return;
                }
                final int lastVisibleItemPosition = findLastVisibleItemPosition();
                int itemsShown = lastVisibleItemPosition - firstVisibleItemPosition + 1;
                //if all items are shown, hide the fast-scroller
                fastScroller.setVisibility(adapter.getItemCount() > itemsShown ? View.VISIBLE : View.GONE);
            }
        });

        fastScroller.setRecyclerView(requestsList);
        fastScroller.setViewsToUse(R.layout.fast_scroller, R.id.fast_scroller_bubble, R.id.fast_scroller_handle);
    }

    public void setRecyclerView(List<User> myConnections, ConnectionsAdapter.OnConnectionsChangedListener onConnectionsChangedListener) {
        adapter = new ConnectionsAdapter(getActivity(), this);
        adapter.setData(myConnections);
        adapter.setListener(onConnectionsChangedListener);

        requestsList.setAdapter(adapter);
    }
}
