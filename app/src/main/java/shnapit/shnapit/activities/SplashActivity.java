package shnapit.shnapit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.facebook.Profile;

import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.UserDbManager;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.responses.SimpleUserResponse;
import shnapit.shnapit.rest.responses.TimestampResponse;
import shnapit.shnapit.rest.services.BasicService;
import shnapit.shnapit.rest.services.UserService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.ProgressFactory;

public class SplashActivity extends AppCompatActivity {

    private long loggedInUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        checkLoggedInStatus();
    }

    private void checkLoggedInStatus() {
        loggedInUserId = PersistentUtils.getLong(this, PersistentUtils.LOGGED_IN_USER_ID, PersistentUtils.NO_USER_ID);
        if (loggedInUserId == PersistentUtils.NO_USER_ID) {
            goToLogin();
            return;
        }

        if (RestClient.hasNetworkConnection(this)) {
            BasicService basicService = RestClient.getInstance().getRetrofit().create(BasicService.class);
            basicService.getTimestamp().enqueue(new Callback<TimestampResponse>() {
                @Override
                public void onResponse(Response<TimestampResponse> response) {
                    PersistentUtils.setLong(SplashActivity.this, response.body().getTimestamp(), PersistentUtils.SERVER_TIMESTAMP);
                    PersistentUtils.setLong(SplashActivity.this, System.currentTimeMillis() / 1000, PersistentUtils.DEVICE_TIMESTAMP);

                    queryUserAndDoWsLogin();
                }

                @Override
                public void onFailure(Throwable t) {
                    goToHome();
                }
            });
            return;
        }

        goToHome();
    }

    private void queryUserAndDoWsLogin() {
        UserDbManager.getInstance().selectUser(loggedInUserId, new ShnapitDatabase.OnResultReceivedListener<User>() {
            @Override
            public void onResultReceived(User result) {
                loginUser(result);
            }
        });
    }

    private void loginUser(final User user) {
        UserService userService = RestClient.getInstance().getRetrofit().create(UserService.class);

        if (!PersistentUtils.getBoolean(this, PersistentUtils.LOGGED_IN_WITH_FACEBOOK, false)) {
            userService.login(new User(user.getEmail(), user.getPassword(), LoginActivity.LOGIN_TYPE_EMAIL, RestClient.getSignature(this, user.getEmail(), RestConstants.USER_LOGIN), PersistentUtils.getString(this, PersistentUtils.DEVICE_TOKEN, null))).enqueue(new Callback<SimpleUserResponse>() {
                @Override
                public void onResponse(Response<SimpleUserResponse> response) {
                    if (response.body() == null || response.body().getUser() == null) {
                        goToHome();
                        return;
                    }

                    PersistentUtils.setString(SplashActivity.this, response.body().getUser().getApiKey(), PersistentUtils.LOGGED_IN_USER_API_TOKEN);
                    response.body().getUser().setPassword(user.getPassword());
                    response.body().getUser().setCategoryIds(TextUtils.join(",", response.body().getUser().getCategoryIdList()));

                    deleteOldUserAndSaveNew(response.body().getUser());
                }

                @Override
                public void onFailure(Throwable t) {
                    goToHome();
                }
            });
            return;
        }

        RestClient.getInstance().getRetrofit().create(UserService.class).login(new User(user.getSocialId(), LoginActivity.LOGIN_TYPE_FACEBOOK, RestClient.getSignature(this, Profile.getCurrentProfile().getId(), RestConstants.USER_LOGIN), PersistentUtils.getString(this, PersistentUtils.DEVICE_TOKEN, null))).enqueue(new Callback<SimpleUserResponse>() {
            @Override
            public void onResponse(Response<SimpleUserResponse> response) {
                if (response.body() == null || response.body().getUser() == null) {
                    goToHome();
                    return;
                }

                PersistentUtils.setString(SplashActivity.this, response.body().getUser().getApiKey(), PersistentUtils.LOGGED_IN_USER_API_TOKEN);
                response.body().getUser().setCategoryIds(TextUtils.join(",", response.body().getUser().getCategoryIdList()));
                if (response.body().getUser().getAccounts() != null && !response.body().getUser().getAccounts().isEmpty())
                    response.body().getUser().setSocialId(response.body().getUser().getAccounts().get(0).getSocialId());

                deleteOldUserAndSaveNew(response.body().getUser());
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(SplashActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void deleteOldUserAndSaveNew(User user) {
        UserDbManager.getInstance().insertUser(user, new ShnapitDatabase.OnResultReceivedListener<Long>() {
            @Override
            public void onResultReceived(Long result) {
                goToHome();
            }
        });
    }

    private void goToLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void goToHome() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
