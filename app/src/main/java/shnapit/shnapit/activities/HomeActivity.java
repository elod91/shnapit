package shnapit.shnapit.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.activities.base.BaseActivity;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.UserDbManager;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.fragments.ConnectionsFragment;
import shnapit.shnapit.fragments.DiscoveryFragment;
import shnapit.shnapit.fragments.FragmentNavigationListener;
import shnapit.shnapit.fragments.SavedFragment;
import shnapit.shnapit.fragments.TermsAndConditionsFragment;
import shnapit.shnapit.fragments.base.BaseFragment;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.request.UpdateCategoriesRequest;
import shnapit.shnapit.rest.responses.ConnectionsResponse;
import shnapit.shnapit.rest.responses.SimpleUserResponse;
import shnapit.shnapit.rest.services.ConnectionService;
import shnapit.shnapit.rest.services.UserService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.ProgressFactory;
import shnapit.shnapit.utils.TagFormat;
import shnapit.shnapit.views.AccountHeaderItem;
import shnapit.shnapit.views.DrawerItem;

public class HomeActivity extends BaseActivity implements Drawer.OnDrawerItemClickListener, FragmentNavigationListener, AccountHeaderItem.OnChangeProfilePictureListener, ImageChooserListener {
    public static final String ACTION_FRIEND_REQUEST = "shnapit.shnapit.activities.HomeActivity.ActionFriendRequest";
    public static final String ACTION_NEW_SHARE = "shnapit.shnapit.activities.HomeActivity.ActionNewShare";
    public static final String MESSAGE_NEW_SHARE = "shnapit.shnapit.activities.HomeActivity.MessageNewShare";

    public static final int REQUEST_PERMISSION_FROM_CAMERA = 0x10;
    public static final int REQUEST_PERMISSION_FROM_GALLERY = 0x11;

    public static final int REQUEST_CATEGORIES = 0x9;

    public static final int DRAWER_IDENTIFIER_DISCOVERY = 0X1;
    public static final int DRAWER_IDENTIFIER_SAVED = 0X2;
    public static final int DRAWER_IDENTIFIER_CATEGORIES = 0X3;
    public static final int DRAWER_IDENTIFIER_MY_CONNECTIONS = 0X4;
    public static final int DRAWER_IDENTIFIER_FEEDBACK = 0X5;
    public static final int DRAWER_IDENTIFIER_TERMS_AND_CONDITIONS = 0X6;
    public static final int DRAWER_IDENTIFIER_DELETE_MY_ACCOUNT = 0X7;
    public static final int DRAWER_IDENTIFIER_LOG_OUT = 0X8;

    private Drawer drawer;
    private DrawerItem connectionsItem;
    private AccountHeaderItem accountHeaderItem;

    private BaseFragment currentFragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    private ImageChooserManager imageChooserManager;

    private User loggedInUser;

    private String filePath;

    private int chooserType;

    private BroadcastReceiver friendRequestReceiver;
    private BroadcastReceiver newShareReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initDrawer();
        getLoggedInUser();
        navigateTo(DiscoveryFragment.class);
        initNewFriendRequestReceiver();
        initNewShareReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetDrawerSelection();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(friendRequestReceiver);
        unregisterReceiver(newShareReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        if (requestCode == REQUEST_CATEGORIES) {
            if (currentFragment instanceof SavedFragment)
                ((SavedFragment) currentFragment).refreshData();
            return;
        }

        if (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE) {
            if (imageChooserManager == null)
                reinitializeImageChooser();

            imageChooserManager.submit(requestCode, data);
            return;
        }

        if (requestCode == Crop.REQUEST_CROP) {
            uploadNewProfilePicture();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_FROM_CAMERA || requestCode == REQUEST_PERMISSION_FROM_GALLERY) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestCode == REQUEST_PERMISSION_FROM_CAMERA) {
                    onTakeNewPicture();
                    return;
                }

                onSelectFromGallery();
            } else {
                DialogFactory.showDialogOneButton(this, getString(R.string.dialog_error_title), getString(R.string.dialog_no_write_permission_message), new DialogFactory.OnButtonPressedListener() {
                    @Override
                    public void onButtonPressed() {
                        if (requestCode == REQUEST_PERMISSION_FROM_CAMERA) {
                            onTakeNewPicture();
                            return;
                        }

                        onSelectFromGallery();
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            closeDrawer();
            return;
        }

        if (!(currentFragment instanceof DiscoveryFragment)) {
            setSelected(DRAWER_IDENTIFIER_DISCOVERY);
            return;
        }

        DialogFactory.showDialog(this, getString(R.string.dialog_exit_application_title), getString(R.string.dialog_exit_application_message), getString(R.string.dialog_yes), getString(R.string.dialog_no), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                supportFinishAfterTransition();
            }

            @Override
            public void onNegative() {
                //Do nothing
            }
        });
    }

    @Override
    public int getToolbarTitle() {
        return currentFragment.getToolbarTitle();
    }

    @Override
    public boolean showRightAction() {
        return currentFragment.showRightAction();
    }

    @Override
    public int getRightActionRes() {
        return currentFragment.getRightActionRes();
    }

    @Override
    public boolean showLeftAction() {
        return currentFragment.showLeftAction();
    }

    @Override
    public int getLeftActionRes() {
        return currentFragment.getLeftActionRes();
    }

    @Override
    public void onRightAction() {
        currentFragment.rightAction();
    }

    @Override
    public void onLeftAction() {
        currentFragment.leftAction();
    }

    @Override
    public void onTitleClicked() {
        currentFragment.titleClicked();
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        if (drawerItem == null)
            return false;

        switch (drawerItem.getIdentifier()) {
            case DRAWER_IDENTIFIER_DISCOVERY:
                navigateTo(DiscoveryFragment.class);
                break;

            case DRAWER_IDENTIFIER_SAVED:
                navigateTo(SavedFragment.class);
                break;

            case DRAWER_IDENTIFIER_CATEGORIES:
                setCategories();
                break;

            case DRAWER_IDENTIFIER_MY_CONNECTIONS:
                navigateTo(ConnectionsFragment.class);
                break;

            case DRAWER_IDENTIFIER_FEEDBACK:
                generateFeedbackEmail();
                break;

            case DRAWER_IDENTIFIER_TERMS_AND_CONDITIONS:
                navigateTo(TermsAndConditionsFragment.class);
                break;

            case DRAWER_IDENTIFIER_DELETE_MY_ACCOUNT:
                deleteUser();
                break;

            case DRAWER_IDENTIFIER_LOG_OUT:
                logOutUser();
        }

        return true;
    }

    @Override
    public <B extends BaseFragment> void navigateTo(Class<B> fragmentClass) {
        if (fragmentManager == null)
            fragmentManager = getSupportFragmentManager();

        try {
            boolean fragmentPopped = fragmentManager.popBackStackImmediate(fragmentClass.getName(), 0);
            if (fragmentPopped) {
                currentFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.home_fragment_container);
                setupToolbar();
                closeDrawer();
                return;
            }

            currentFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.home_fragment_container);
            if (currentFragment != null && fragmentClass.getName().equals(currentFragment.getClass().getName())) {
                closeDrawer();
                return;
            }


            currentFragment = fragmentClass.newInstance();
            currentFragment.setNavigationListener(this);

            transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(fragmentClass.getName());
            transaction.add(R.id.home_fragment_container, currentFragment, fragmentClass.getName());
            transaction.commit();

            setupToolbar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setCategories() {
        closeDrawer();
        resetDrawerSelection();

        if (!RestClient.hasNetworkConnection(this)) {
            DialogFactory.showDialogOneButton(this, getString(R.string.dialog_error_title), getString(R.string.dialog_no_internet_error_message), null);
            return;
        }

        Intent intent = new Intent(this, CategoriesActivity.class);
        intent.putExtra(CategoriesActivity.CAME_FROM, CategoriesActivity.FROM_HOME);

        startActivityForResult(intent, REQUEST_CATEGORIES);
        overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                navigateTo(SavedFragment.class);
            }
        }, 300);
    }

    @Override
    public void setSelected(int identifier) {
        if (drawer != null)
            drawer.setSelection(identifier);
    }

    @Override
    public void toggleDrawer() {
        if (drawer == null)
            return;

        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
            return;
        }

        drawer.openDrawer();
    }

    @Override
    public void closeDrawer() {
        if (drawer == null)
            return;

        if (drawer.isDrawerOpen())
            drawer.closeDrawer();
    }

    @Override
    public void connectionsChanged() {
        setRequestsBadge();
    }

    @Override
    public void onTakeNewPicture() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_FROM_CAMERA);
            return;
        }

        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;

        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);

        try {
            filePath = imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSelectFromGallery() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_FROM_GALLERY);
            return;
        }

        chooserType = ChooserType.REQUEST_PICK_PICTURE;

        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();

        try {
            filePath = imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onImageChosen(ChosenImage chosenImage) {
        filePath = chosenImage.getFilePathOriginal();

        Crop.of(Uri.fromFile(new File(filePath)), Uri.fromFile(new File(filePath))).asSquare().start(this);
    }

    @Override
    public void onError(String s) {

    }

    private void getLoggedInUser() {
        long loggedInUserId = PersistentUtils.getLong(this, PersistentUtils.LOGGED_IN_USER_ID, -1);
        if (loggedInUserId == -1) {
            setupDrawer();
            return;
        }

        UserDbManager.getInstance().selectUser(loggedInUserId, new ShnapitDatabase.OnResultReceivedListener<User>() {
            @Override
            public void onResultReceived(User result) {
                loggedInUser = result;
                setupDrawer();
            }
        });
    }

    private void initNewFriendRequestReceiver() {
        friendRequestReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setRequestsBadge();
                if (currentFragment instanceof ConnectionsFragment)
                    ((ConnectionsFragment) currentFragment).getMyConnections();
            }
        };
        registerReceiver(friendRequestReceiver, new IntentFilter(ACTION_FRIEND_REQUEST));
    }

    private void initNewShareReceiver() {
        newShareReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent == null)
                    return;

                final String message = intent.getStringExtra(MESSAGE_NEW_SHARE);
                if (message == null)
                    return;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();

                        if (currentFragment instanceof SavedFragment)
                            ((SavedFragment) currentFragment).showNewPosts();
                    }
                });
            }
        };
        registerReceiver(newShareReceiver, new IntentFilter(ACTION_NEW_SHARE));
    }

    private void initDrawer() {
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withActionBarDrawerToggle(false)
                .withOnDrawerItemClickListener(this)
                .withSliderBackgroundColorRes(R.color.colorPrimary)
                .build();
    }

    private void setupDrawer() {
        connectionsItem = generateDrawerItem(getString(R.string.drawer_connections), DRAWER_IDENTIFIER_MY_CONNECTIONS);

        if (loggedInUser != null) {
            drawer.addItems(generateAccountHeader(loggedInUser), generateDrawerItem(getString(R.string.drawer_discover), DRAWER_IDENTIFIER_DISCOVERY), generateDrawerItem(getString(R.string.drawer_saved), DRAWER_IDENTIFIER_SAVED), generateDrawerItem(getString(R.string.drawer_categories), DRAWER_IDENTIFIER_CATEGORIES), generateDrawerItem("", 0).withEnabled(false), connectionsItem, generateDrawerItem("", 0).withEnabled(false), generateDrawerItem(getString(R.string.drawer_feedback), DRAWER_IDENTIFIER_FEEDBACK), generateDrawerItem(getString(R.string.terms_and_conditions_title), DRAWER_IDENTIFIER_TERMS_AND_CONDITIONS), generateDrawerItem(getString(R.string.drawer_delete_my_account), DRAWER_IDENTIFIER_DELETE_MY_ACCOUNT), generateDrawerItem(getString(R.string.drawer_log_out), DRAWER_IDENTIFIER_LOG_OUT));
            setSelected(DRAWER_IDENTIFIER_DISCOVERY);
            setRequestsBadge();
            return;
        }

        loggedInUser = new User();
        loggedInUser.setFirstName("Unknown");
        loggedInUser.setUserName("Unknown");
        drawer.addItems(generateAccountHeader(loggedInUser), generateDrawerItem(getString(R.string.drawer_discover), DRAWER_IDENTIFIER_DISCOVERY), generateDrawerItem(getString(R.string.drawer_saved), DRAWER_IDENTIFIER_SAVED), generateDrawerItem(getString(R.string.drawer_categories), DRAWER_IDENTIFIER_CATEGORIES), generateDrawerItem("", 0).withEnabled(false), connectionsItem, generateDrawerItem("", 0).withEnabled(false), generateDrawerItem(getString(R.string.drawer_feedback), DRAWER_IDENTIFIER_FEEDBACK), generateDrawerItem(getString(R.string.terms_and_conditions_title), DRAWER_IDENTIFIER_TERMS_AND_CONDITIONS), generateDrawerItem(getString(R.string.drawer_delete_my_account), DRAWER_IDENTIFIER_DELETE_MY_ACCOUNT), generateDrawerItem(getString(R.string.drawer_log_out), DRAWER_IDENTIFIER_LOG_OUT));
        setSelected(DRAWER_IDENTIFIER_DISCOVERY);
        setRequestsBadge();
    }

    private void setRequestsBadge() {
        String apiToken = PersistentUtils.getString(this, PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);
        RestClient.getInstance().getRetrofit().create(ConnectionService.class).getConnections(apiToken, RestClient.getSignature(this, apiToken, TagFormat.from(RestConstants.CONNECTIONS_GENERAL).with("api_token", apiToken).format())).enqueue(new Callback<ConnectionsResponse>() {
            @Override
            public void onResponse(Response<ConnectionsResponse> response) {
                if (response.body() == null)
                    return;

                if (response.body().getPendingConnectionRequests().isEmpty()) {
                    connectionsItem.setBadge(null);
                    drawer.updateItem(connectionsItem);
                    return;
                }

                connectionsItem.setBadge(String.format("%d", response.body().getPendingConnectionRequests().size()));
                drawer.updateItem(connectionsItem);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    private void uploadNewProfilePicture() {
        long loggedInUserId = PersistentUtils.getLong(HomeActivity.this, PersistentUtils.LOGGED_IN_USER_ID, -1);
        if (loggedInUserId == -1)
            return;

        ProgressFactory.showProgressDialog(HomeActivity.this);
        UserDbManager.getInstance().selectUser(loggedInUserId, new ShnapitDatabase.OnResultReceivedListener<User>() {
            @Override
            public void onResultReceived(final User result) {
                final List<Long> categoryIds = new ArrayList<>();
                for (String categoryId : result.getCategoryIds().split(","))
                    categoryIds.add(Long.parseLong(categoryId));

                RequestBody imageFile = RequestBody.create(MediaType.parse("image/png"), new File(filePath));

                RestClient.getInstance().getRetrofit().create(UserService.class).uploadNewProfilePhoto(result.getApiKey(), imageFile, generateRequestBodyForParams(RestClient.getSignature(HomeActivity.this, result.getApiKey(), TagFormat.from(RestConstants.UPDATE_USER).with("api_token", result.getApiKey()).format())), generateRequestBodyForParams(result.getFirstName()), generateRequestBodyForParams(result.getLastName()), generateRequestBodyForParams("")).enqueue(new Callback<SimpleUserResponse>() {
                    @Override
                    public void onResponse(Response<SimpleUserResponse> response) {
                        if (response.body() == null) {
                            ProgressFactory.hideProgressDialog();
                            RestClient.handleErrors(HomeActivity.this, response.errorBody());
                            return;
                        }

                        result.setProfileImage(response.body().getUser().getProfileImage());

                        UserDbManager.getInstance().insertUser(result, null);
                        accountHeaderItem.setPhotoUrl(response.body().getUser().getProfileImage());
                        drawer.updateItem(accountHeaderItem);

                        RestClient.getInstance().getRetrofit().create(UserService.class).updateUserCategories(result.getApiKey(), new UpdateCategoriesRequest(result.getFirstName(), result.getLastName(), TextUtils.join(", ", categoryIds), RestClient.getSignature(HomeActivity.this, result.getApiKey(), TagFormat.from(RestConstants.UPDATE_USER).with("api_token", result.getApiKey()).format()))).enqueue(new Callback<SimpleUserResponse>() {
                            @Override
                            public void onResponse(Response<SimpleUserResponse> response) {
                                ProgressFactory.hideProgressDialog();
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                ProgressFactory.hideProgressDialog();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        ProgressFactory.hideProgressDialog();
                        if (t instanceof JsonSyntaxException)
                            return;

                        DialogFactory.showDialogOneButton(HomeActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
                    }
                });
            }
        });
    }

    private AccountHeaderItem generateAccountHeader(User user) {
        if (accountHeaderItem == null)
            accountHeaderItem = new AccountHeaderItem(this)
                    .withFullName(String.format("%s %s", user.getFirstName(), user.getLastName()))
                    .withUserName(user.getUserName() == null ? user.getEmail() : user.getUserName())
                    .withPhotoUrl(user.getProfileImage())
                    .withListener(this)
                    .withEnabled(false);

        return accountHeaderItem;
    }

    private DrawerItem generateDrawerItem(String title, int identifier) {
        return new DrawerItem()
                .withTitle(title)
                .withIdentifier(identifier);
    }

    private void generateFeedbackEmail() {
        closeDrawer();
        resetDrawerSelection();

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.feedback_email_address)});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_email_subject));
        emailIntent.putExtra(Intent.EXTRA_TEXT, TagFormat.from(getString(R.string.feedback_email_body)).with("user_name", String.format("%s %s", loggedInUser.getFirstName(), loggedInUser.getLastName())).format());

        startActivity(Intent.createChooser(emailIntent, getString(R.string.feedback_chooser_title)));
    }

    private void deleteUser() {
        closeDrawer();
        resetDrawerSelection();

        DialogFactory.showDialog(this, getString(R.string.dialog_delete_account_title), getString(R.string.dialog_delete_account_message), getString(R.string.dialog_yes), getString(R.string.dialog_no), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                RestClient.getInstance().getRetrofit().create(UserService.class).deleteUser(loggedInUser.getApiKey(), RestClient.getSignature(HomeActivity.this, loggedInUser.getApiKey(), TagFormat.from(RestConstants.GET_CATEGORIES).with("api_token", loggedInUser.getApiKey()).format())).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Response<Object> response) {
                        if (response.body() == null) {
                            RestClient.handleErrors(HomeActivity.this, response.errorBody());
                            return;
                        }

                        clearSharedPreferencesAndGoToLogin();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        DialogFactory.showDialogOneButton(HomeActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
                    }
                });
            }

            @Override
            public void onNegative() {

            }
        });
    }

    private void logOutUser() {
        closeDrawer();
        resetDrawerSelection();

        DialogFactory.showDialog(this, getString(R.string.dialog_confirm_title), getString(R.string.dialog_log_out_message), getString(R.string.dialog_yes), getString(R.string.dialog_no), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                clearSharedPreferencesAndGoToLogin();
            }

            @Override
            public void onNegative() {

            }
        });
    }

    private void clearSharedPreferencesAndGoToLogin() {
        PersistentUtils.clearSharedPreferences(this);

        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void resetDrawerSelection() {
        if (currentFragment.getClass().getName().equals(DiscoveryFragment.class.getName())) {
            drawer.setSelection(DRAWER_IDENTIFIER_DISCOVERY);
            return;
        }

        if (currentFragment.getClass().getName().equals(SavedFragment.class.getName())) {
            drawer.setSelection(DRAWER_IDENTIFIER_SAVED);
            return;
        }

        if (currentFragment.getClass().getName().equals(ConnectionsFragment.class.getName())) {
            drawer.setSelection(DRAWER_IDENTIFIER_MY_CONNECTIONS);
            return;
        }

        if (currentFragment.getClass().getName().equals(TermsAndConditionsFragment.class.getName()))
            drawer.setSelection(DRAWER_IDENTIFIER_TERMS_AND_CONDITIONS);
    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    private RequestBody generateRequestBodyForParams(String param) {
        return RequestBody.create(MediaType.parse("text/plain"), param);
    }
}
