package shnapit.shnapit.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.activities.base.BaseActivity;
import shnapit.shnapit.adapters.ReshareAdapter;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.ProductDbManager;
import shnapit.shnapit.database.models.Product;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.responses.ConnectionsResponse;
import shnapit.shnapit.rest.services.ConnectionService;
import shnapit.shnapit.rest.services.ProductService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.ProgressFactory;
import shnapit.shnapit.utils.TagFormat;

public class ReshareProductActivity extends BaseActivity {
    public static final String RESHARE_PRODUCT_ID = "net.shnapit.activities.ReshareProductActivity.ReshareProductId";

    private RecyclerView reshareList;
    private ReshareAdapter adapter;

    private Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reshare_product);

        initList();
        getProduct();
        getConnections();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
    }

    @Override
    public boolean showLeftActionText() {
        return true;
    }

    @Override
    public boolean showRightActionText() {
        return true;
    }

    @Override
    public int getLeftActionTextColor() {
        return ContextCompat.getColor(this, R.color.loginBlue);
    }

    @Override
    public int getRightActionTextColor() {
        return ContextCompat.getColor(this, R.color.loginBlue);
    }

    @Override
    public String getRightActionText() {
        return getString(R.string.toolbar_share);
    }

    @Override
    public void onRightAction() {
        if (adapter.getSelectedUserIds().isEmpty())
            return;

        ProgressFactory.showProgressDialog(this);
        String apiToken = PersistentUtils.getString(this, PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);

        RestClient.getInstance().getRetrofit().create(ProductService.class).reshareProduct(apiToken, product.getId(), "in-app", adapter.getSelectedUserIds(), RestClient.getSignature(this, apiToken, TagFormat.from(RestConstants.PRODUCT_RESHARE).with("api_token", apiToken).with("id", product.getId()).format())).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Response<Object> response) {
                ProgressFactory.hideProgressDialog();
                if (response.body() == null) {
                    RestClient.handleErrors(ReshareProductActivity.this, response.errorBody());
                    return;
                }
                finish();
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                if (t instanceof JsonSyntaxException) {
                    finish();
                    return;
                }
                DialogFactory.showDialogOneButton(ReshareProductActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    @Override
    public void onLeftAction() {
        finish();
        overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
    }

    private void initList() {
        reshareList = (RecyclerView) findViewById(R.id.reshare_contacts_list);
    }

    private void getProduct() {
        long productId = getIntent().getLongExtra(RESHARE_PRODUCT_ID, -1);
        if (productId == -1)
            return;

        ProductDbManager.getInstance().selectProduct(productId, new ShnapitDatabase.OnResultReceivedListener<Product>() {
            @Override
            public void onResultReceived(Product result) {
                product = result;
            }
        });
    }

    private void getConnections() {
        String apiToken = PersistentUtils.getString(this, PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);

        RestClient.getInstance().getRetrofit().create(ConnectionService.class).getConnections(apiToken, RestClient.getSignature(this, apiToken, TagFormat.from(RestConstants.CONNECTIONS_GENERAL).with("api_token", apiToken).format())).enqueue(new Callback<ConnectionsResponse>() {
            @Override
            public void onResponse(Response<ConnectionsResponse> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(ReshareProductActivity.this, response.errorBody());
                    setListWithData(new ArrayList<User>());
                    return;
                }

                if (response.body().getAcceptedConnections() == null || response.body().getAcceptedConnections().isEmpty()) {
                    setListWithData(new ArrayList<User>());
                    return;
                }

                setListWithData(response.body().getAcceptedConnections());
            }

            @Override
            public void onFailure(Throwable t) {
                DialogFactory.showDialogOneButton(ReshareProductActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
                setListWithData(new ArrayList<User>());
            }
        });
    }

    private void setListWithData(List<User> connections) {
        adapter = new ReshareAdapter(connections);

        reshareList.setLayoutManager(new LinearLayoutManager(this));
        reshareList.setAdapter(adapter);
    }
}
