package shnapit.shnapit.activities;

import android.Manifest;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import shnapit.shnapit.R;
import shnapit.shnapit.application.ShnapitApplication;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.GeneralUtil;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WebPictureActivity extends AppCompatActivity implements View.OnClickListener, TextView.OnEditorActionListener {
    public static final int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 0x1;

    private WebView webView;
    private RelativeLayout hintContainer, bottomContainer;
    private ScrollView previewScroll;
    private LinearLayout previewContainer;
    private EditText urlInput;
    private TextView chooseImage;

    private List<String> imageUrls;
    private String savedUrl;

    private BroadcastReceiver downloadReceiver;

    private DownloadManager downloadManager;
    private String filePath;

    private long enqueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_picture);

        initViews();
        setOnClickListeners(findViewById(R.id.web_picture_post_product), findViewById(R.id.web_picture_close));
        initWebView();
        setDownloadReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(downloadReceiver);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (previewScroll.getVisibility() == View.VISIBLE) {
            hidePreview();
            return;
        }

        finish();
        overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        if (requestCode == Crop.REQUEST_CROP) {
            Intent intent = new Intent(this, EditProductActivity.class);
            intent.putExtra(EditProductActivity.IMAGE_PATH, filePath);
            intent.putExtra(EditProductActivity.OPERATION, EditProductActivity.OPERATION_CREATE);

            startActivity(intent);
            overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveImage(savedUrl);
            } else {
                DialogFactory.showDialogOneButton(this, getString(R.string.dialog_error_title), getString(R.string.dialog_no_write_permission_message), new DialogFactory.OnButtonPressedListener() {
                    @Override
                    public void onButtonPressed() {
                        saveImage(savedUrl);
                    }
                });
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.web_picture_post_product:
                showPreview();
                break;

            case R.id.web_picture_close:
                onBackPressed();
                break;

            default:
                saveImage(v.getTag().toString());
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
            GeneralUtil.hideSoftKeyboard(WebPictureActivity.this);

            String url = urlInput.getText().toString();
            if (!url.startsWith("www.") && !url.startsWith("http://") && !url.startsWith("https://"))
                url = "www." + url;
            if (!url.startsWith("http://") && !url.startsWith("https://"))
                url = "http://" + url;

            webView.loadUrl(url);
            webView.setVisibility(View.VISIBLE);

            hintContainer.setVisibility(View.GONE);
            return true;
        }
        return false;
    }

    private void initViews() {
        webView = (WebView) findViewById(R.id.web_picture_browser);

        hintContainer = (RelativeLayout) findViewById(R.id.web_picture_hint_text_container);
        bottomContainer = (RelativeLayout) findViewById(R.id.web_picture_bottom_button_container);

        previewScroll = (ScrollView) findViewById(R.id.web_picture_preview_scroll);

        previewContainer = (LinearLayout) findViewById(R.id.web_picture_preview_container);

        urlInput = (EditText) findViewById(R.id.web_picture_url_input);
        urlInput.setOnEditorActionListener(this);

        chooseImage = (TextView) findViewById(R.id.web_picture_choose_image);
    }

    private void setOnClickListeners(View... views) {
        for (View view : views)
            view.setOnClickListener(this);
    }

    private void initWebView() {
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new ProductWebClient());
    }

    private void setDownloadReceiver() {
        downloadReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!intent.getAction().equalsIgnoreCase(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
                    return;

                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(enqueue);
                Cursor c = downloadManager.query(query);
                if (c.moveToFirst())
                    if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS)))
                        startCropImage();
            }
        };
    }

    private void showPreview() {
        previewScroll.setVisibility(View.VISIBLE);
        chooseImage.setVisibility(View.VISIBLE);

        bottomContainer.setVisibility(View.GONE);
        urlInput.setVisibility(View.GONE);

        for (int i = 0; i < imageUrls.size(); ) {
            RelativeLayout previewRow = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.row_preview, previewContainer, false);

            ImageView firstPreview = (ImageView) previewRow.findViewById(R.id.preview_first_image);
            ImageView secondPreview = (ImageView) previewRow.findViewById(R.id.preview_second_image);

            Picasso.with(this).load(imageUrls.get(i)).into(firstPreview);
            firstPreview.setTag(imageUrls.get(i));
            firstPreview.setOnClickListener(this);

            if (i == imageUrls.size() - 1) {
                secondPreview.setImageResource(R.color.blackTransparent);

                previewContainer.addView(previewRow);
                break;
            }
            i++;

            Picasso.with(this).load(imageUrls.get(i)).into(secondPreview);
            secondPreview.setTag(imageUrls.get(i));
            secondPreview.setOnClickListener(this);

            previewContainer.addView(previewRow);
            i++;
        }
    }

    private void hidePreview() {
        previewScroll.setVisibility(View.GONE);
        chooseImage.setVisibility(View.GONE);

        bottomContainer.setVisibility(View.VISIBLE);
        urlInput.setVisibility(View.VISIBLE);
    }

    private void saveImage(String url) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            savedUrl = url;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE);
            return;
        }

        filePath = Environment.getExternalStorageDirectory() + ShnapitApplication.SAVE_FOLDER;

        File saveFolder = new File(filePath);
        if (!saveFolder.exists())
            saveFolder.mkdir();

        String fileName = url.split("/")[url.split("/").length - 1];
        filePath = String.format("%s/%s", filePath, fileName);

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI
                | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(getString(R.string.app_name))
                .setDescription(getString(R.string.app_name))
                .setDestinationInExternalPublicDir(ShnapitApplication.SAVE_FOLDER, fileName);

        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        enqueue = downloadManager.enqueue(request);
    }

    private void startCropImage() {
        Crop.of(Uri.fromFile(new File(filePath)), Uri.fromFile(new File(filePath))).asSquare().start(this);
    }

    public class ProductWebClient extends WebViewClient {
        private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|jpeg))$)";

        Pattern pattern;

        public ProductWebClient() {
            pattern = Pattern.compile(IMAGE_PATTERN);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            urlInput.setText(url);
            imageUrls = new ArrayList<>();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);

            if (pattern.matcher(url).matches())
                imageUrls.add(url);

            bottomContainer.setVisibility(View.VISIBLE);
        }
    }
}
