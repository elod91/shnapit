package shnapit.shnapit.activities;

import android.os.Bundle;

import shnapit.shnapit.R;
import shnapit.shnapit.activities.base.BaseActivity;

public class TermsAndConditionsActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public int getToolbarTitle() {
        return R.string.terms_and_conditions_title;
    }

    @Override
    public boolean showRightAction() {
        return true;
    }

    @Override
    public int getRightActionRes() {
        return R.drawable.selector_close;
    }

    @Override
    public void onRightAction() {
        super.onBackPressed();
    }
}
