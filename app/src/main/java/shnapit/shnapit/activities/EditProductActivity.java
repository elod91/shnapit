package shnapit.shnapit.activities;

import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonSyntaxException;
import com.squareup.picasso.Picasso;
import com.squareup.seismic.ShakeDetector;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.activities.base.BaseActivity;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.ProductDbManager;
import shnapit.shnapit.database.models.Product;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.services.ProductService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.FileUtils;
import shnapit.shnapit.utils.GeneralUtil;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.ProgressFactory;
import shnapit.shnapit.utils.SaveBitmapToFile;
import shnapit.shnapit.utils.TagFormat;
import shnapit.shnapit.views.ColorPickerDialog;
import shnapit.shnapit.views.HashEditText;
import shnapit.shnapit.views.PaintView;

public class EditProductActivity extends BaseActivity implements ShakeDetector.Listener {
    public static final String IMAGE_PATH = "net.shnapit.activities.EditProductActivity.ImagePath";
    public static final String OPERATION = "net.shnapit.activities.EditProductActivity.Operation";
    public static final String PRODUCT_ID = "net.shnapit.activities.EditProductActivity.ProductId";

    public static final int REQUEST_CODE_GET_CATEGORY = 0x3;

    public static final int OPERATION_CREATE = 0x1;
    public static final int OPERATION_EDIT = 0x2;

    private RelativeLayout paintContainer;
    private LinearLayout infoSlider;
    private ImageView paintMode, textMode;
    private EditText brandName, productName, productPrice, productWebsite;
    private HashEditText hashtag;
    private PaintView paint;

    private Animation inFromBottom, outToBottom;

    private String imagePath;
    private long productId, categoryId = -1;
    private int operation;
    private boolean undoDialogShowing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        initViews();
        setOnClickListeners(paintMode, textMode, findViewById(R.id.edit_product_color), findViewById(R.id.edit_product_open_information), findViewById(R.id.edit_product_set_category), findViewById(R.id.edit_product_close_slider));
        setHashListeners();
        useIntentData();
        initAnimations();
        initShake();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        if (requestCode == REQUEST_CODE_GET_CATEGORY)
            categoryId = data.getLongExtra(CategoriesActivity.SELECTED_CATEGORY_ID, -1);
    }

    @Override
    public void onBackPressed() {
        askForGoBack();
    }

    @Override
    public int getToolbarTitle() {
        return R.string.app_name;
    }

    @Override
    public boolean showRightActionText() {
        return true;
    }

    @Override
    public boolean showLeftActionText() {
        return true;
    }

    @Override
    public void onRightAction() {
        getEditedAndSaveProductImage();
    }

    @Override
    public void onLeftAction() {
        askForGoBack();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_product_color:
                showColorPicker();
                break;

            case R.id.edit_product_paint_mode:
                setPaintModeActive();
                break;

            case R.id.edit_product_text_mode:
                setTextModeActive();
                break;

            case R.id.edit_product_open_information:
                openInfoSlider();
                break;

            case R.id.edit_product_set_category:
                categories();
                break;

            case R.id.edit_product_close_slider:
                closeInfoSlider();
                break;
            default:
                super.onClick(v);
        }
    }

    @Override
    public void hearShake() {
        if (undoDialogShowing)
            return;

        undoDialogShowing = true;
        DialogFactory.showDialog(this, getString(R.string.edit_product_undo_dialog_title), getString(R.string.edit_product_undo_dialog_message), getString(R.string.dialog_yes), getString(R.string.dialog_no), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                setPaintModeActive();
                paint.undoChanges();

                undoDialogShowing = false;
            }

            @Override
            public void onNegative() {
                undoDialogShowing = false;
            }
        });
    }

    private void initViews() {
        paintContainer = (RelativeLayout) findViewById(R.id.edit_product_paint_container);

        infoSlider = (LinearLayout) findViewById(R.id.edit_product_info_slider);

        paintMode = (ImageView) findViewById(R.id.edit_product_paint_mode);
        textMode = (ImageView) findViewById(R.id.edit_product_text_mode);

        brandName = (EditText) findViewById(R.id.edit_product_brand_name);
        productName = (EditText) findViewById(R.id.edit_product_product_name);
        productPrice = (EditText) findViewById(R.id.edit_product_product_price);
        productWebsite = (EditText) findViewById(R.id.edit_product_product_website);

        hashtag = (HashEditText) findViewById(R.id.edit_product_hashtag);

        paint = (PaintView) findViewById(R.id.edit_product_paint);
    }

    private void setOnClickListeners(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    private void setHashListeners() {
        hashtag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getKeyCode() == KeyEvent.KEYCODE_BACK)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    setPaintModeActive();
                }

                return false;
            }
        });
        hashtag.setHashBackPressListener(new HashEditText.HashBackPressListener() {
            @Override
            public void onHashBackPress() {
                setPaintModeActive();
            }
        });
    }

    private void useIntentData() {
        String imagePath = getIntent().getStringExtra(IMAGE_PATH);

        operation = getIntent().getIntExtra(OPERATION, OPERATION_CREATE);
        if (operation == OPERATION_EDIT) {
            productId = getIntent().getLongExtra(PRODUCT_ID, -1);
            Picasso.with(this).load(imagePath).into((ImageView) findViewById(R.id.edit_product_image));
            ProductDbManager.getInstance().selectProduct(productId, new ShnapitDatabase.OnResultReceivedListener<Product>() {
                @Override
                public void onResultReceived(Product result) {
                    categoryId = result.getCategoryId();

                    brandName.setText(result.getBrand());
                    productName.setText(result.getName());
                    productPrice.setText(result.getPrice());
                    productWebsite.setText(result.getWebsite());
                }
            });
            return;
        }

        Picasso.with(this).load(new File(imagePath)).into((ImageView) findViewById(R.id.edit_product_image));
    }

    private void initAnimations() {
        inFromBottom = AnimationUtils.loadAnimation(this, R.anim.in_from_bottom);
        outToBottom = AnimationUtils.loadAnimation(this, R.anim.out_to_bottom);

        outToBottom.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                infoSlider.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void initShake() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector shakeDetector = new ShakeDetector(this);
        shakeDetector.start(sensorManager);
    }

    private void showColorPicker() {
        DialogFactory.showColorPickerDialog(this, new ColorPickerDialog.OnColorChangedListener() {
            @Override
            public void colorChanged(int color) {
                paint.setDrawColor(color);
            }
        });
    }

    private void setPaintModeActive() {
        GeneralUtil.hideSoftKeyboard(EditProductActivity.this);

        paintMode.setSelected(true);
        textMode.setSelected(false);

        paint.setActive(true);

        hashtag.setVisibility(View.GONE);
    }

    private void setTextModeActive() {
        paintMode.setSelected(false);
        textMode.setSelected(true);

        paint.setActive(false);

        hashtag.setVisibility(View.VISIBLE);
        if (hashtag.requestFocus()) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(hashtag.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    private void categories() {
        Intent categories = new Intent(this, CategoriesActivity.class);
        categories.putExtra(CategoriesActivity.CAME_FROM, CategoriesActivity.FROM_EDIT);

        startActivityForResult(categories, REQUEST_CODE_GET_CATEGORY);

        overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);
    }

    private void openInfoSlider() {
        infoSlider.setVisibility(View.VISIBLE);
        infoSlider.startAnimation(inFromBottom);
    }

    private void closeInfoSlider() {
        infoSlider.startAnimation(outToBottom);
    }

    private void getEditedAndSaveProductImage() {
        if (!RestClient.hasNetworkConnection(this)) {
            DialogFactory.showDialog(this, getString(R.string.dialog_error_title), getString(R.string.dialog_no_internet_error_message), null);
            return;
        }

        if (categoryId == -1) {
            DialogFactory.showDialogOneButton(this, getString(R.string.edit_product_no_category_dialog_title), getString(R.string.edit_product_no_category_dialog_message), null);
            return;
        }

        ProgressFactory.showProgressDialog(this);

        paintContainer.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        paintContainer.setDrawingCacheEnabled(true);
        paintContainer.buildDrawingCache(true);


        imagePath = FileUtils.getInstance().getNewEditedProductFilePath();
        FileUtils.getInstance().saveBitmapToFile(imagePath, paintContainer.getDrawingCache());
        new SaveBitmapToFile(imagePath, paintContainer.getDrawingCache(), new SaveBitmapToFile.OnSaveFinishedListener() {
            @Override
            public void onSaveFinished() {
                if (operation == OPERATION_CREATE) {
                    createProduct();
                    return;
                }
                updateProduct();
            }
        }).execute();
    }

    private void createProduct() {
        String apiToken = PersistentUtils.getString(this, PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);

        RequestBody imageFile = RequestBody.create(MediaType.parse("image/*"), new File(imagePath));

        RestClient.getInstance().getRetrofit().create(ProductService.class).createProduct(apiToken, imageFile, generateRequestBodyForParams(RestClient.getSignatureForUpload(apiToken, TagFormat.from(RestConstants.PRODUCT_GENERAL + ".json").with("api_token", apiToken).format())), generateRequestBodyForParams(hashtag.getText().toString()), generateRequestBodyForParams(String.format("%d", categoryId)), generateRequestBodyForParams(brandName.getText().toString()), generateRequestBodyForParams(productName.getText().toString()), generateRequestBodyForParams(productPrice.getText().toString()), generateRequestBodyForParams(productWebsite.getText().toString())).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Response<Product> response) {
                ProgressFactory.hideProgressDialog();
                if (response.body() == null) {
                    RestClient.handleErrors(EditProductActivity.this, response.errorBody());
                    return;
                }
                finish();
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                if (t instanceof JsonSyntaxException) {
                    finish();
                    return;
                }
                DialogFactory.showDialogOneButton(EditProductActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void updateProduct() {
        String apiToken = PersistentUtils.getString(this, PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);

        RequestBody imageFile = RequestBody.create(MediaType.parse("image/*"), new File(imagePath));

        RestClient.getInstance().getRetrofit().create(ProductService.class).updateProduct(apiToken, productId, imageFile, generateRequestBodyForParams(RestClient.getSignatureForUpload(apiToken, TagFormat.from(RestConstants.PRODUCT_GENERAL + "/" + productId + ".json").with("api_token", apiToken).format())), generateRequestBodyForParams(hashtag.getText().toString()), generateRequestBodyForParams(String.format("%d", categoryId)), generateRequestBodyForParams(brandName.getText().toString()), generateRequestBodyForParams(productName.getText().toString()), generateRequestBodyForParams(productPrice.getText().toString()), generateRequestBodyForParams(productWebsite.getText().toString()), null).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Response<Product> response) {
                ProgressFactory.hideProgressDialog();
                if (response.body() == null) {
                    RestClient.handleErrors(EditProductActivity.this, response.errorBody());
                    return;
                }
                finish();
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                if (t instanceof JsonSyntaxException) {
                    finish();
                    return;
                }
                DialogFactory.showDialogOneButton(EditProductActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void askForGoBack() {
        DialogFactory.showDialog(this, getString(R.string.edit_product_cancel_edit_title), getString(R.string.edit_product_cancel_edit_message), getString(R.string.dialog_positive_btn), getString(R.string.dialog_negative_btn), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                finish();
                overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
            }

            @Override
            public void onNegative() {
                //Do nothing
            }
        });
    }

    private RequestBody generateRequestBodyForParams(String param) {
        return RequestBody.create(MediaType.parse("text/plain"), param);
    }
}
