package shnapit.shnapit.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.UserDbManager;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.gcm.RegistrationIntentService;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.request.LocationRequest;
import shnapit.shnapit.rest.request.UpdateCategoriesRequest;
import shnapit.shnapit.rest.responses.CategoriesResponse;
import shnapit.shnapit.rest.responses.Category;
import shnapit.shnapit.rest.responses.ForgotPasswordResponse;
import shnapit.shnapit.rest.responses.SimpleUserResponse;
import shnapit.shnapit.rest.responses.TimestampResponse;
import shnapit.shnapit.rest.services.BasicService;
import shnapit.shnapit.rest.services.UserService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.PlayServicesUtils;
import shnapit.shnapit.utils.ProgressFactory;
import shnapit.shnapit.utils.TagFormat;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, Animation.AnimationListener, DatePickerDialog.OnDateSetListener, View.OnFocusChangeListener, GoogleApiClient.OnConnectionFailedListener, FacebookCallback<LoginResult> {
    public static final String LOGIN_TYPE_EMAIL = "email";
    public static final String LOGIN_TYPE_FACEBOOK = "facebook";

    public static final int REQUEST_PERMISSION_LOCATION = 0x1;

    private RelativeLayout facebookContainer, forgotAndRegisterContainer;
    private LinearLayout registerInputContainer;
    private TextView registerAndBack;
    private EditText firstName, lastName, username, birthday, email, password;
    private Button loginWithFacebook, loginAndRegister;
    private DatePickerDialog datePickerDialog;
    private AlertDialog forgotPasswordDialog;

    private Animation inFromLeft, outToLeft, inFromRight, outToRight, inFromLeftRegister, outToLeftRegister;

    private GoogleApiClient googleApiClient;
    private User registeredUser;

    private CallbackManager callbackManager;

    private String deviceToken, facebookEmail, facebookBirthday;

    private boolean registerShowing = false, requestForgotPassword = false, requestCheckUsernameAvailability = false, googleApiConnectionSuccess = true, requestFacebook = false;

    private BroadcastReceiver registrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            deviceToken = intent.getStringExtra(RegistrationIntentService.TOKEN);
            PersistentUtils.setString(LoginActivity.this, deviceToken, PersistentUtils.DEVICE_TOKEN);
            getTimestamp();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "shnapit.shnapit",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }

        initViews();
        setOnClickListeners(registerAndBack, findViewById(R.id.login_forgot_password), birthday, loginWithFacebook, loginAndRegister, findViewById(R.id.login_terms_and_conditions), findViewById(R.id.login_privacy_policy));
        initAnimations();
        initFacebook();
        LocalBroadcastManager.getInstance(this).registerReceiver(registrationBroadcastReceiver, new IntentFilter(RegistrationIntentService.TOKEN_ACTION));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        if (googleApiClient != null)
            googleApiClient.disconnect();

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(registrationBroadcastReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getAndSendLocation();
            } else {
                sendNoLocation();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_register_and_back:
                switchLoginAndRegister();
                break;

            case R.id.login_forgot_password:
                if (registerShowing) {
                    switchLoginAndRegister();
                    break;
                }
                showForgotPasswordDialog();
                break;

            case R.id.login_birthday:
                datePickerDialog.show();
                break;

            case R.id.login_log_in_with_facebook:
                loginWithFacebook();
                break;

            case R.id.login_log_in_with_shnapit:
                if (registerShowing) {
                    register();
                    break;
                }
                login();
                break;

            case R.id.login_terms_and_conditions:
                openTermsAndConditions();
                break;

            case R.id.login_privacy_policy:
                openTermsAndConditions();
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        registerInputContainer.setVisibility(registerShowing ? View.VISIBLE : View.INVISIBLE);
        facebookContainer.setVisibility(registerShowing ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        birthday.setText(String.format("%d.%d.%d", monthOfYear + 1, dayOfMonth, year));
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.login_birthday:
                if (hasFocus)
                    datePickerDialog.show();
                break;

            case R.id.login_username:
                if (!hasFocus) {
                    requestCheckUsernameAvailability = true;
                    getTimestamp();
                }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        googleApiConnectionSuccess = false;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    facebookEmail = object.getString("email");

                    requestFacebook = true;
                    getTimestamp();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onError(FacebookException error) {
    }

    private void initViews() {
        facebookContainer = (RelativeLayout) findViewById(R.id.login_facebook_container);
        forgotAndRegisterContainer = (RelativeLayout) findViewById(R.id.login_forgot_and_register_container);

        registerInputContainer = (LinearLayout) findViewById(R.id.login_register_input_container);

        registerAndBack = (TextView) findViewById(R.id.login_register_and_back);

        firstName = (EditText) findViewById(R.id.login_first_name);
        lastName = (EditText) findViewById(R.id.login_last_name);
        username = (EditText) findViewById(R.id.login_username);
        birthday = (EditText) findViewById(R.id.login_birthday);
        email = (EditText) findViewById(R.id.login_email);
        password = (EditText) findViewById(R.id.login_password);

        birthday.setOnFocusChangeListener(this);
        username.setOnFocusChangeListener(this);

        loginWithFacebook = (Button) findViewById(R.id.login_log_in_with_facebook);
        loginAndRegister = (Button) findViewById(R.id.login_log_in_with_shnapit);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        datePickerDialog = new DatePickerDialog(this, this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    private void setOnClickListeners(View... views) {
        for (View view : views)
            view.setOnClickListener(this);
    }

    private void initAnimations() {
        inFromLeft = AnimationUtils.loadAnimation(this, R.anim.in_from_left);
        outToLeft = AnimationUtils.loadAnimation(this, R.anim.out_to_left);
        inFromRight = AnimationUtils.loadAnimation(this, R.anim.in_from_right);
        outToRight = AnimationUtils.loadAnimation(this, R.anim.out_to_right);

        inFromLeftRegister = AnimationUtils.loadAnimation(this, R.anim.in_from_left_register);
        outToLeftRegister = AnimationUtils.loadAnimation(this, R.anim.out_to_left_register);

        setAnimationListeners(inFromLeft, outToLeft, inFromRight, outToRight);
    }

    private void initFacebook() {
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    public void setAnimationListeners(Animation... animations) {
        for (Animation animation : animations)
            animation.setAnimationListener(this);
    }

    private void switchLoginAndRegister() {
        registerInputContainer.setVisibility(View.VISIBLE);
        facebookContainer.setVisibility(View.VISIBLE);

        if (registerShowing) {
            registerInputContainer.startAnimation(outToRight);
            facebookContainer.startAnimation(inFromLeft);

            registerAndBack.setText(R.string.login_register);
            registerAndBack.setOnClickListener(this);
            forgotAndRegisterContainer.startAnimation(inFromLeftRegister);

            loginAndRegister.setText(R.string.login_general_login);
            email.setHint(R.string.login_hint_username_email);

            registerShowing = !registerShowing;
            return;
        }

        registerInputContainer.startAnimation(inFromRight);
        facebookContainer.startAnimation(outToLeft);

        registerAndBack.setText(R.string.login_back);
        registerAndBack.setOnClickListener(null);
        forgotAndRegisterContainer.startAnimation(outToLeftRegister);

        loginAndRegister.setText(R.string.login_register);
        email.setHint(R.string.login_hint_email);

        registerShowing = !registerShowing;
    }

    private void openTermsAndConditions() {
        startActivity(new Intent(this, TermsAndConditionsActivity.class));
    }

    private void loginWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    private void login() {
        if (!checkParams(email, password)) {
            DialogFactory.showDialogOneButton(this, getString(R.string.dialog_error_title), getString(R.string.dialog_empty_fields_error), null);
            return;
        }

        getTimestamp();
    }

    private void register() {
        if (!checkParams(firstName, lastName, username, birthday, email, password)) {
            DialogFactory.showDialogOneButton(this, getString(R.string.dialog_error_title), getString(R.string.dialog_empty_fields_error), null);
            return;
        }

        getTimestamp();
    }

    private void showForgotPasswordDialog() {
        if (forgotPasswordDialog == null)
            forgotPasswordDialog = new AlertDialog.Builder(this, R.style.AlertDialog_AppCompat_Light_Shnapit)
                    .setView(R.layout.dialog_forgot_password)
                    .setTitle(R.string.login_forgot_password_title)
                    .setCancelable(false)
                    .setPositiveButton(R.string.dialog_positive_btn, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestForgotPassword = true;
                            getTimestamp();
                        }
                    })
                    .setNegativeButton(R.string.dialog_negative_btn, null)
                    .create();

        forgotPasswordDialog.show();
    }

    private void getTimestamp() {
        if (RestClient.hasNetworkConnection(this)) {
            ProgressFactory.showProgressDialog(this);
            if (deviceToken == null)
                deviceToken = PersistentUtils.getString(this, PersistentUtils.DEVICE_TOKEN, null);
            if (deviceToken == null && PlayServicesUtils.checkPlayServices(this)) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
                return;
            }

            BasicService basicService = RestClient.getInstance().getRetrofit().create(BasicService.class);
            basicService.getTimestamp().enqueue(new Callback<TimestampResponse>() {
                @Override
                public void onResponse(Response<TimestampResponse> response) {
                    PersistentUtils.setLong(LoginActivity.this, response.body().getTimestamp(), PersistentUtils.SERVER_TIMESTAMP);
                    PersistentUtils.setLong(LoginActivity.this, System.currentTimeMillis() / 1000, PersistentUtils.DEVICE_TIMESTAMP);

                    if (requestFacebook) {
                        doFacebookLogin(true);
                        return;
                    }

                    if (requestCheckUsernameAvailability) {
                        checkUsernameAvailability();
                        return;
                    }

                    if (requestForgotPassword) {
                        continueForgotPassword();
                        return;
                    }

                    if (registerShowing) {
                        continueRegister();
                        return;
                    }

                    continueLogin();
                }

                @Override
                public void onFailure(Throwable t) {
                    DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
                }
            });
            return;
        }

        DialogFactory.showDialogOneButton(this, getString(R.string.dialog_error_title), getString(R.string.dialog_no_internet_error_message), null);
    }

    private void doFacebookLogin(final boolean tryToRegister) {
        requestFacebook = false;
        if (tryToRegister) {
            RestClient.getInstance().getRetrofit().create(UserService.class).registerUser(new User(facebookEmail, Profile.getCurrentProfile().getId(), Profile.getCurrentProfile().getFirstName(), Profile.getCurrentProfile().getLastName(), RestClient.getSignature(this, Profile.getCurrentProfile().getId(), RestConstants.USER_REGISTER), LOGIN_TYPE_FACEBOOK, deviceToken)).enqueue(new Callback<SimpleUserResponse>() {
                @Override
                public void onResponse(Response<SimpleUserResponse> response) {
                    if (response.isSuccess())
                        registerShowing = true;
                    doFacebookLogin(false);
                }

                @Override
                public void onFailure(Throwable t) {
                    ProgressFactory.hideProgressDialog();
                    DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
                }
            });
            return;
        }

        RestClient.getInstance().getRetrofit().create(UserService.class).login(new User(Profile.getCurrentProfile().getId(), LOGIN_TYPE_FACEBOOK, RestClient.getSignature(this, Profile.getCurrentProfile().getId(), RestConstants.USER_LOGIN), deviceToken)).enqueue(new Callback<SimpleUserResponse>() {
            @Override
            public void onResponse(Response<SimpleUserResponse> response) {
                handleResult(response);
                PersistentUtils.setBoolean(LoginActivity.this, true, PersistentUtils.LOGGED_IN_WITH_FACEBOOK);
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void continueLogin() {
        RestClient.getInstance().getRetrofit().create(UserService.class).login(new User(email.getText().toString(), password.getText().toString(), LOGIN_TYPE_EMAIL, RestClient.getSignature(this, email.getText().toString(), RestConstants.USER_LOGIN), deviceToken)).enqueue(new Callback<SimpleUserResponse>() {
            @Override
            public void onResponse(Response<SimpleUserResponse> response) {
                handleResult(response);
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void continueRegister() {
        RestClient.getInstance().getRetrofit().create(UserService.class).registerUser(new User(username.getText().toString(), email.getText().toString(), password.getText().toString(), firstName.getText().toString(), lastName.getText().toString(), RestClient.getSignature(this, username.getText().toString(), RestConstants.USER_REGISTER), LOGIN_TYPE_EMAIL, deviceToken)).enqueue(new Callback<SimpleUserResponse>() {
            @Override
            public void onResponse(Response<SimpleUserResponse> response) {
                PersistentUtils.setBoolean(LoginActivity.this, true, PersistentUtils.SHOW_DROPDOWN);

                handleResult(response);
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void continueForgotPassword() {
        requestForgotPassword = false;

        String email = ((EditText) forgotPasswordDialog.findViewById(R.id.login_forgot_password_email)).getText().toString();
        ((EditText) forgotPasswordDialog.findViewById(R.id.login_forgot_password_email)).setText(null);

        RestClient.getInstance().getRetrofit().create(UserService.class).forgotPassword(new User(email, RestClient.getSignature(this, email, RestConstants.USER_FORGOT_PASSWORD))).enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Response<ForgotPasswordResponse> response) {
                ProgressFactory.hideProgressDialog();

                if (response.body() == null) {
                    RestClient.handleErrors(LoginActivity.this, response.errorBody());
                    return;
                }

                DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.login_recovered_password_title), response.body().getMessage(), null);
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void checkUsernameAvailability() {
        requestCheckUsernameAvailability = false;

        RestClient.getInstance().getRetrofit().create(UserService.class).checkUsernameAvailability(username.getText().toString(), RestClient.getSignature(this, username.getText().toString(), RestConstants.USER_VALIDATE_USERNAME)).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Response<Object> response) {
                ProgressFactory.hideProgressDialog();

                if (response.body() != null) {
                    username.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                    return;
                }

                username.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_clear, 0);
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void handleResult(Response<SimpleUserResponse> response) {
        if (response.body() == null) {
            ProgressFactory.hideProgressDialog();
            RestClient.handleErrors(this, response.errorBody());
            return;
        }

        registeredUser = response.body().getUser();

        if (response.body().getUser().getCategoryIdList() != null)
            response.body().getUser().setCategoryIds(TextUtils.join(",", response.body().getUser().getCategoryIdList()));

        if (response.body().getUser().getAccounts() != null && !response.body().getUser().getAccounts().isEmpty())
            response.body().getUser().setSocialId(response.body().getUser().getAccounts().get(0).getSocialId());

        response.body().getUser().setPassword(password.getText().toString());
        UserDbManager.getInstance().insertUser(response.body().getUser(), new ShnapitDatabase.OnResultReceivedListener<Long>() {
            @Override
            public void onResultReceived(Long result) {
                PersistentUtils.setLong(LoginActivity.this, result, PersistentUtils.LOGGED_IN_USER_ID);
                PersistentUtils.setString(LoginActivity.this, registeredUser.getApiKey(), PersistentUtils.LOGGED_IN_USER_API_TOKEN);

                registeredUser.setPassword(password.getText().toString());
                if (registerShowing) {
                    getAllCategories();
                    return;
                }

                goToHome();
            }
        });
    }

    private void getUsersLocation() {
        DialogFactory.showDialog(this, getString(R.string.login_location_dialog_title), getString(R.string.login_location_dialog_message), getString(R.string.dialog_positive_btn), getString(R.string.login_location_dialog_deny), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                getAndSendLocation();
            }

            @Override
            public void onNegative() {
                sendNoLocation();
            }
        });
    }

    private void getAndSendLocation() {
        if (googleApiConnectionSuccess) {
            if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_LOCATION);
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            RestClient.getInstance().getRetrofit().create(UserService.class).sendUserLocation(registeredUser.getApiKey(), new LocationRequest(1, location.getLatitude(), location.getLongitude(), RestClient.getSignature(this, registeredUser.getApiKey(), TagFormat.from(RestConstants.USER_SEND_LOCATION).with("api_token", registeredUser.getApiKey()).format()))).enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Response<Object> response) {

                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

            return;
        }

        sendNoLocation();
    }

    private void sendNoLocation() {
        RestClient.getInstance().getRetrofit().create(UserService.class).sendUserLocation(registeredUser.getApiKey(), new LocationRequest(0, RestClient.getSignature(this, registeredUser.getApiKey(), TagFormat.from(RestConstants.USER_SEND_LOCATION).with("api_token", registeredUser.getApiKey()).format()))).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Response<Object> response) {

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        getAllCategories();
    }

    private void getAllCategories() {
        RestClient.getInstance().getRetrofit().create(UserService.class).getAllCategories(registeredUser.getApiKey(), RestClient.getSignature(this, registeredUser.getApiKey(), TagFormat.from(RestConstants.GET_CATEGORIES).with("api_token", registeredUser.getApiKey()).format())).enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Response<CategoriesResponse> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(LoginActivity.this, response.errorBody());
                    return;
                }

                setCategoriesForUser(response.body().getCategories());
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void setCategoriesForUser(List<Category> categories) {
        List<Long> categoryIds = new ArrayList<>();
        for (Category category : categories)
            categoryIds.add(category.getId());

        RestClient.getInstance().getRetrofit().create(UserService.class).updateUserCategories(registeredUser.getApiKey(), new UpdateCategoriesRequest(registeredUser.getFirstName(), registeredUser.getLastName(), TextUtils.join(", ", categoryIds), RestClient.getSignature(this, registeredUser.getApiKey(), TagFormat.from(RestConstants.UPDATE_USER).with("api_token", registeredUser.getApiKey()).format()))).enqueue(new Callback<SimpleUserResponse>() {
            @Override
            public void onResponse(Response<SimpleUserResponse> response) {
                response.body().getUser().setCategoryIds(TextUtils.join(",", response.body().getUser().getCategoryIdList()));
                response.body().getUser().setPassword(registeredUser.getPassword());

                UserDbManager.getInstance().insertUser(response.body().getUser(), new ShnapitDatabase.OnResultReceivedListener<Long>() {
                    @Override
                    public void onResultReceived(Long result) {
                        goToHome();
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(LoginActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void goToHome() {
        ProgressFactory.hideProgressDialog();

        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    private boolean checkParams(EditText... inputs) {
        boolean paramsOk = true;
        for (EditText input : inputs)
            if (input.getText().toString().isEmpty())
                paramsOk = false;

        return paramsOk;
    }
}
