package shnapit.shnapit.activities;

public interface ActivityToolbarInterface {
    int getToolbarTitle();

    String getToolbarTitleText();

    boolean showRightAction();

    int getRightActionRes();

    boolean showLeftAction();

    boolean showRightActionText();

    boolean showLeftActionText();

    int getRightActionTextColor();

    int getLeftActionTextColor();

    String getRightActionText();

    String getLeftActionText();

    int getLeftActionRes();

    void onRightAction();

    void onLeftAction();

    void onTitleClicked();
}
