package shnapit.shnapit.activities.base;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import shnapit.shnapit.R;
import shnapit.shnapit.activities.ActivityToolbarInterface;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, ActivityToolbarInterface {

    private Toolbar toolbar;
    private TextView toolbarTitle, toolbarRightActionText, toolbarLeftActionText;
    private ImageView toolbarRightAction, toolbarLeftAction;

    @Override
    protected void onResume() {
        super.onResume();

        setupToolbar();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_right_action:
                onRightAction();
                break;

            case R.id.toolbar_left_action:
                onLeftAction();
                break;

            case R.id.toolbar_right_action_text:
                onRightAction();
                break;

            case R.id.toolbar_left_action_text:
                onLeftAction();
                break;

            case R.id.toolbar_title:
                onTitleClicked();
        }
    }

    @Override
    public int getToolbarTitle() {
        return R.string.app_name;
    }

    @Override
    public String getToolbarTitleText() {
        return getString(R.string.app_name);
    }

    @Override
    public boolean showRightAction() {
        return false;
    }

    @Override
    public int getRightActionRes() {
        return 0;
    }

    @Override
    public boolean showLeftAction() {
        return false;
    }

    @Override
    public int getLeftActionRes() {
        return 0;
    }

    @Override
    public boolean showRightActionText() {
        return false;
    }

    @Override
    public boolean showLeftActionText() {
        return false;
    }

    @Override
    public int getRightActionTextColor() {
        return Color.RED;
    }

    @Override
    public int getLeftActionTextColor() {
        return Color.GREEN;
    }

    @Override
    public String getRightActionText() {
        return getString(R.string.toolbar_save);
    }

    @Override
    public String getLeftActionText() {
        return getString(R.string.toolbar_cancel);
    }

    @Override
    public void onRightAction() {

    }

    @Override
    public void onLeftAction() {

    }

    @Override
    public void onTitleClicked() {

    }

    protected void setupToolbar() {
        initToolbar();
        setToolbarTitle();
        setToolbarRightAction();
        setToolbarLeftAction();
        setToolbarRightActionText();
        setToolbarLeftActionText();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.shnapit_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarRightActionText = (TextView) toolbar.findViewById(R.id.toolbar_right_action_text);
        toolbarLeftActionText = (TextView) toolbar.findViewById(R.id.toolbar_left_action_text);

        toolbarRightAction = (ImageView) toolbar.findViewById(R.id.toolbar_right_action);
        toolbarLeftAction = (ImageView) toolbar.findViewById(R.id.toolbar_left_action);
    }

    protected void setToolbarTitle() {
        if (getToolbarTitle() != 0) {
            toolbarTitle.setText(getToolbarTitle());
            toolbarTitle.setOnClickListener(this);
            return;
        }

        if (getToolbarTitleText() != null) {
            toolbarTitle.setText(getToolbarTitleText());
            toolbarTitle.setOnClickListener(this);
        }
    }

    protected void setToolbarRightAction() {
        if (showRightAction()) {
            toolbarRightAction.setVisibility(View.VISIBLE);
            toolbarRightAction.setImageResource(getRightActionRes());
            toolbarRightAction.setOnClickListener(this);
            return;
        }

        toolbarRightAction.setVisibility(View.INVISIBLE);
        toolbarRightAction.setOnClickListener(null);
    }

    protected void setToolbarLeftAction() {
        if (showLeftAction()) {
            toolbarLeftAction.setVisibility(View.VISIBLE);
            toolbarLeftAction.setImageResource(getLeftActionRes());
            toolbarLeftAction.setOnClickListener(this);
            return;
        }

        toolbarLeftAction.setVisibility(View.INVISIBLE);
        toolbarLeftAction.setOnClickListener(null);
    }

    protected void setToolbarRightActionText() {
        if (showRightActionText()) {
            toolbarRightActionText.setVisibility(View.VISIBLE);
            toolbarRightActionText.setOnClickListener(this);
            toolbarRightActionText.setText(getRightActionText());
            toolbarRightActionText.setTextColor(getRightActionTextColor());
            return;
        }

        toolbarRightActionText.setVisibility(View.GONE);
        toolbarRightActionText.setOnClickListener(null);
    }

    protected void setToolbarLeftActionText() {
        if (showLeftActionText()) {
            toolbarLeftActionText.setVisibility(View.VISIBLE);
            toolbarLeftActionText.setOnClickListener(this);
            toolbarLeftActionText.setText(getLeftActionText());
            toolbarLeftActionText.setTextColor(getLeftActionTextColor());
            return;
        }

        toolbarLeftActionText.setVisibility(View.GONE);
        toolbarLeftActionText.setOnClickListener(null);
    }

    protected Toolbar getToolbar() {
        return toolbar;
    }
}
