package shnapit.shnapit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.activities.base.BaseActivity;
import shnapit.shnapit.adapters.CategoriesAdapter;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.UserDbManager;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.request.UpdateCategoriesRequest;
import shnapit.shnapit.rest.responses.CategoriesResponse;
import shnapit.shnapit.rest.responses.Category;
import shnapit.shnapit.rest.responses.SimpleUserResponse;
import shnapit.shnapit.rest.services.UserService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.ProgressFactory;
import shnapit.shnapit.utils.TagFormat;

public class CategoriesActivity extends BaseActivity {
    public static final String CAME_FROM = "net.shnapit.activities.CategoriesActivity.CameFrom";
    public static final String SELECTED_CATEGORY_ID = "net.shnapit.activities.CategoriesActivity.SelectedCategoryId";

    public static final int FROM_EDIT = 0x1;
    public static final int FROM_HOME = 0x2;

    private RecyclerView categoryList;
    private CategoriesAdapter adapter;

    private int cameFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        initRecyclerView();
        getCategories();
    }

    @Override
    public void onBackPressed() {
        onLeftAction();
    }

    @Override
    public int getToolbarTitle() {
        return R.string.categories_title;
    }

    @Override
    public boolean showLeftAction() {
        return true;
    }

    @Override
    public int getLeftActionRes() {
        return R.drawable.ic_back;
    }

    @Override
    public void onLeftAction() {
        switch (cameFrom) {
            case FROM_HOME:
                saveNewCategoriesAndGoBack();
                break;

            case FROM_EDIT:
                getSelectedCategoryAndGoBack();
        }
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        categoryList = (RecyclerView) findViewById(R.id.categories_list);
        categoryList.setLayoutManager(layoutManager);
    }

    private void getCategories() {
        ProgressFactory.showProgressDialog(this);

        String apiToken = PersistentUtils.getString(this, PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);
        RestClient.getInstance().getRetrofit().create(UserService.class).getAllCategories(apiToken, RestClient.getSignature(this, apiToken, TagFormat.from(RestConstants.GET_CATEGORIES).with("api_token", apiToken).format())).enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Response<CategoriesResponse> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(CategoriesActivity.this, response.errorBody(), new DialogFactory.OnButtonPressedListener() {
                        @Override
                        public void onButtonPressed() {
                            setResult(RESULT_OK);
                            finish();
                            overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
                        }
                    });
                    return;
                }

                handleData(response.body());
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressFactory.hideProgressDialog();
                DialogFactory.showDialogOneButton(CategoriesActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), new DialogFactory.OnButtonPressedListener() {
                    @Override
                    public void onButtonPressed() {
                        setResult(RESULT_OK);
                        finish();
                        overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
                    }
                });
            }
        });
    }

    private void handleData(CategoriesResponse categoriesResponse) {
        cameFrom = getIntent().getIntExtra(CAME_FROM, FROM_HOME);

        switch (cameFrom) {
            case FROM_HOME:
                setAdapter(categoriesResponse);
                ((TextView) findViewById(R.id.categories_label)).setText(R.string.categories_from_home_label);
                break;

            case FROM_EDIT:
                setListForEdit(categoriesResponse);
                ((TextView) findViewById(R.id.categories_label)).setText(R.string.categories_label_from_edit);
        }
    }

    private void setListForEdit(final CategoriesResponse categoriesResponse) {
        for (Category category : categoriesResponse.getCategories())
            category.setSelected(false);

        setAdapter(categoriesResponse);
    }

    private void setAdapter(CategoriesResponse categoriesResponse) {
        adapter = new CategoriesAdapter(this, cameFrom, categoriesResponse.getCategories());
        categoryList.setAdapter(adapter);

        ProgressFactory.hideProgressDialog();
    }

    private void saveNewCategoriesAndGoBack() {
        DialogFactory.showDialog(this, getString(R.string.dialog_categories_update_title), getString(R.string.dialog_categories_update_message), getString(R.string.dialog_yes), getString(R.string.dialog_no), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                boolean selectAll = false;
                if (adapter.getData().get(0).isSelected())
                    selectAll = true;

                final List<Long> categoryIds = new ArrayList<>();
                for (int i = 1; i < adapter.getData().size(); i++)
                    if (selectAll || adapter.getData().get(i).isSelected())
                        categoryIds.add(adapter.getData().get(i).getId());

                long loggedInUserId = PersistentUtils.getLong(CategoriesActivity.this, PersistentUtils.LOGGED_IN_USER_ID, -1);
                if (loggedInUserId == -1) {
                    DialogFactory.showDialogOneButton(CategoriesActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), new DialogFactory.OnButtonPressedListener() {
                        @Override
                        public void onButtonPressed() {
                            setResult(RESULT_OK);
                            finish();
                            overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
                        }
                    });
                    return;
                }

                UserDbManager.getInstance().selectUser(loggedInUserId, new ShnapitDatabase.OnResultReceivedListener<User>() {
                    @Override
                    public void onResultReceived(final User result) {
                        if (result == null) {
                            DialogFactory.showDialogOneButton(CategoriesActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), new DialogFactory.OnButtonPressedListener() {
                                @Override
                                public void onButtonPressed() {
                                    setResult(RESULT_OK);
                                    finish();
                                    overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
                                }
                            });
                            return;
                        }

                        RestClient.getInstance().getRetrofit().create(UserService.class).updateUserCategories(result.getApiKey(), new UpdateCategoriesRequest(result.getFirstName(), result.getLastName(), TextUtils.join(", ", categoryIds), RestClient.getSignature(CategoriesActivity.this, result.getApiKey(), TagFormat.from(RestConstants.UPDATE_USER).with("api_token", result.getApiKey()).format()))).enqueue(new Callback<SimpleUserResponse>() {
                            @Override
                            public void onResponse(Response<SimpleUserResponse> response) {
                                if (response.body() == null) {
                                    RestClient.handleErrors(CategoriesActivity.this, response.errorBody(), new DialogFactory.OnButtonPressedListener() {
                                        @Override
                                        public void onButtonPressed() {
                                            setResult(RESULT_OK);
                                            finish();
                                            overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
                                        }
                                    });
                                    return;
                                }

                                response.body().getUser().setCategoryIds(TextUtils.join(",", response.body().getUser().getCategoryIdList()));
                                response.body().getUser().setPassword(result.getPassword());

                                UserDbManager.getInstance().insertUser(response.body().getUser(), new ShnapitDatabase.OnResultReceivedListener<Long>() {
                                    @Override
                                    public void onResultReceived(Long result) {
                                        setResult(RESULT_OK);
                                        finish();
                                        overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
                                    }
                                });
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                ProgressFactory.hideProgressDialog();
                                DialogFactory.showDialogOneButton(CategoriesActivity.this, getString(R.string.dialog_error_title), getString(R.string.dialog_general_error_message), new DialogFactory.OnButtonPressedListener() {
                                    @Override
                                    public void onButtonPressed() {
                                        setResult(RESULT_OK);
                                        finish();
                                        overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
                                    }
                                });
                            }
                        });
                    }
                });
            }

            @Override
            public void onNegative() {
                setResult(RESULT_OK);
                finish();
                overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
            }
        });
    }

    private void getSelectedCategoryAndGoBack() {
        long selectedCategoryId = -1;
        for (Category category : adapter.getData())
            if (category.isSelected()) {
                selectedCategoryId = category.getId();
                break;
            }

        Intent resultData = new Intent();
        resultData.putExtra(SELECTED_CATEGORY_ID, selectedCategoryId);

        setResult(RESULT_OK, resultData);
        finish();

        overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
    }
}
