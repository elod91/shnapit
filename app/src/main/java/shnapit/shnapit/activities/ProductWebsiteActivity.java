package shnapit.shnapit.activities;

import android.os.Bundle;
import android.webkit.WebView;

import shnapit.shnapit.R;
import shnapit.shnapit.activities.base.BaseActivity;

public class ProductWebsiteActivity extends BaseActivity {
    public static final String PRODUCT_WEBSITE = "net.shnapit.activities.ProductWebsiteActivity.ProductWebsite";
    public static final String PRODUCT_NAME = "net.shnapit.activities.ProductWebsiteActivity.ProductName";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_website);
        initWebView();
    }

    @Override
    public void onBackPressed() {
        onLeftAction();
    }

    @Override
    public int getToolbarTitle() {
        return 0;
    }

    @Override
    public String getToolbarTitleText() {
        return getIntent().getStringExtra(PRODUCT_NAME);
    }

    @Override
    public boolean showLeftAction() {
        return true;
    }

    @Override
    public int getLeftActionRes() {
        return R.drawable.ic_back;
    }

    @Override
    public void onLeftAction() {
        finish();
        overridePendingTransition(R.anim.zoom_in, R.anim.out_to_right_activity);
    }

    private void initWebView() {
        WebView webView = (WebView) findViewById(R.id.product_website_view);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);

        String url = getIntent().getStringExtra(PRODUCT_WEBSITE);
        webView.loadUrl(url);
    }
}
