package shnapit.shnapit.database;

@com.raizlabs.android.dbflow.annotation.Database(name = ShnapitDatabase.NAME, version = ShnapitDatabase.VERSION)
public class ShnapitDatabase {
    public static final String NAME = "shnapit";
    public static final int VERSION = 1;

    public interface OnResultReceivedListener<T> {
        void onResultReceived(T result);
    }
}
