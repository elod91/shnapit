package shnapit.shnapit.database.managers;

import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.runtime.transaction.SelectListTransaction;
import com.raizlabs.android.dbflow.runtime.transaction.TransactionListenerAdapter;
import com.raizlabs.android.dbflow.runtime.transaction.process.ProcessModelInfo;
import com.raizlabs.android.dbflow.runtime.transaction.process.SaveModelTransaction;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.List;

import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.models.Product;
import shnapit.shnapit.database.models.Product_Table;

public class ProductDbManager {

    private static ProductDbManager INSTANCE;

    private ProductDbManager() {

    }

    public static synchronized ProductDbManager getInstance() {
        if (INSTANCE == null)
            INSTANCE = new ProductDbManager();

        return INSTANCE;
    }

    public void insertMultipleProducts(Product[] products, final ShnapitDatabase.OnResultReceivedListener<List<Product>> onResultReceivedListener) {
        ProcessModelInfo<Product> productProcessModelInfo = ProcessModelInfo.withModels(products).result(new TransactionListenerAdapter<List<Product>>() {
            @Override
            public void onResultReceived(List<Product> resultProducts) {
                super.onResultReceived(resultProducts);
                if (onResultReceivedListener == null)
                    return;

                onResultReceivedListener.onResultReceived(resultProducts);
            }
        });

        TransactionManager.getInstance().addTransaction(new SaveModelTransaction<>(productProcessModelInfo));
    }

    public void selectAllProducts(final ShnapitDatabase.OnResultReceivedListener<List<Product>> onResultReceivedListener) {
        TransactionManager.getInstance().addTransaction(
                new SelectListTransaction<>(new Select().from(Product.class).where(),
                        new TransactionListenerAdapter<List<Product>>() {
                            @Override
                            public void onResultReceived(List<Product> products) {
                                if (products != null)
                                    onResultReceivedListener.onResultReceived(products);

                                super.onResultReceived(products);
                            }
                        })
        );
    }

    public void selectProduct(long productId, final ShnapitDatabase.OnResultReceivedListener<Product> onResultReceivedListener) {
        new Select()
                .from(Product.class)
                .where(Product_Table.id.is(productId))
                .async()
                .querySingle(new TransactionListenerAdapter<Product>() {
                    @Override
                    public void onResultReceived(Product product) {
                        onResultReceivedListener.onResultReceived(product);
                    }
                });
    }
}
