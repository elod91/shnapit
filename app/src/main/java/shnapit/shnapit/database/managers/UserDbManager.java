package shnapit.shnapit.database.managers;

import android.database.Cursor;

import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.runtime.transaction.SelectListTransaction;
import com.raizlabs.android.dbflow.runtime.transaction.SelectSingleModelTransaction;
import com.raizlabs.android.dbflow.runtime.transaction.TransactionListenerAdapter;
import com.raizlabs.android.dbflow.runtime.transaction.process.ProcessModelInfo;
import com.raizlabs.android.dbflow.runtime.transaction.process.SaveModelTransaction;
import com.raizlabs.android.dbflow.sql.language.Delete;

import java.util.List;

import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.database.models.User_Table;

public class UserDbManager {

    private static UserDbManager INSTANCE;

    private UserDbManager() {
    }

    public static synchronized UserDbManager getInstance() {
        if (INSTANCE == null)
            INSTANCE = new UserDbManager();

        return INSTANCE;
    }

    public void selectUser(long id, final ShnapitDatabase.OnResultReceivedListener<User> onResultReceivedListener) {
        TransactionManager.getInstance().addTransaction(
                new SelectSingleModelTransaction<>(User.class,
                        new TransactionListenerAdapter<User>() {
                            @Override
                            public void onResultReceived(User user) {

                                if (onResultReceivedListener != null)
                                    onResultReceivedListener.onResultReceived(user);
                            }
                        }, User_Table.id.is(id)
                )
        );
    }

    public void selectAllUsersExcludingOne(long excludeUserId, final ShnapitDatabase.OnResultReceivedListener<List<User>> onResultReceivedListener) {
        TransactionManager.getInstance().addTransaction(
                new SelectListTransaction<>(new TransactionListenerAdapter<List<User>>() {
                    @Override
                    public void onResultReceived(List<User> users) {
                        if (users != null)
                            onResultReceivedListener.onResultReceived(users);
                        super.onResultReceived(users);
                    }
                }, User.class, User_Table.id.isNot(excludeUserId)));
    }

    public void insertUser(User user, final ShnapitDatabase.OnResultReceivedListener<Long> onResultReceivedListener) {
        ProcessModelInfo<User> userProcessModelInfo = ProcessModelInfo.withModels(user).result(new TransactionListenerAdapter<List<User>>() {
            @Override
            public void onResultReceived(List<User> users) {
                super.onResultReceived(users);

                if (onResultReceivedListener != null)
                    onResultReceivedListener.onResultReceived(users == null || users.isEmpty() ? null : users.get(0).getId());
            }
        });

        TransactionManager.getInstance().addTransaction(new SaveModelTransaction<>(userProcessModelInfo));
    }

    public void insertMultipleUsers(User[] users, final ShnapitDatabase.OnResultReceivedListener<List<User>> onResultReceivedListener) {
        ProcessModelInfo<User> userProcessModelInfo = ProcessModelInfo.withModels(users).result(new TransactionListenerAdapter<List<User>>() {
            @Override
            public void onResultReceived(List<User> users) {
                super.onResultReceived(users);
                if (onResultReceivedListener == null)
                    return;

                onResultReceivedListener.onResultReceived(users);
            }
        });

        TransactionManager.getInstance().addTransaction(new SaveModelTransaction<>(userProcessModelInfo));
    }

    public void deleteAllUsers(final ShnapitDatabase.OnResultReceivedListener<Void> onResultReceivedListener) {
        new Delete().from(User.class).async().query(new TransactionListenerAdapter<Cursor>() {
            @Override
            public void onResultReceived(Cursor cursor) {
                super.onResultReceived(cursor);

                if (onResultReceivedListener != null)
                    onResultReceivedListener.onResultReceived(null);
            }
        });
    }

    public void deleteUser(long userId, final ShnapitDatabase.OnResultReceivedListener<Void> onResultReceivedListener) {
        new Delete()
                .from(User.class)
                .where(User_Table.id.is(userId))
                .async()
                .query(new TransactionListenerAdapter<Cursor>() {
                    @Override
                    public void onResultReceived(Cursor cursor) {
                        super.onResultReceived(cursor);

                        if (onResultReceivedListener != null)
                            onResultReceivedListener.onResultReceived(null);
                    }
                });
    }
}
