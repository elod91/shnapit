package shnapit.shnapit.database.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

import shnapit.shnapit.database.ShnapitDatabase;

@Table(database = ShnapitDatabase.class)
public class User extends BaseModel {
    public static final String DEVICE_TYPE_ANDROID = "android";

    public static final int CONNECTION_STATUS_ACCEPTED = 0x1;
    public static final int CONNECTION_STATUS_PENDING = 0x2;
    public static final int CONNECTION_STATUS_PENDING_REQUESTS = 0x3;

    @Column
    @Expose
    @PrimaryKey(autoincrement = false)
    Long id;

    @Column
    @Expose
    @SerializedName("user_name")
    String userName;

    @Column
    @Expose
    @SerializedName("user_id")
    String userId;

    @Column
    @Expose
    String email;

    @Column
    @Expose
    String password;

    @Column
    @Expose
    @SerializedName("first_name")
    String firstName;

    @Column
    @Expose
    @SerializedName("last_name")
    String lastName;

    @Expose
    @SerializedName("fullname")
    String fullName;

    @Column
    @Expose
    @SerializedName("image")
    String profileImage;

    @Column
    @Expose
    @SerializedName("api_token")
    String apiKey;

    @Column
    String categoryIds;


    @Column
    int connectionStatus;

    @Expose
    @SerializedName("category_ids")
    List<Long> categoryIdList;

    @Expose
    String signature;

    @Expose
    @SerializedName("device_token")
    String deviceToken;

    @Expose
    @SerializedName("device")
    String deviceType;

    @Expose
    @SerializedName("registration_type")
    String registrationType;

    @Expose
    @SerializedName("social_id")
    @Column
    String socialId;

    @Expose
    @SerializedName("social_provider")
    String socialProvider;

    boolean alphabet;

    String alphabetValue;

    boolean checkedForReshare;

    @Expose
    List<Account> accounts;

    public User() {
    }

    /**
     * Constructor used for login.
     *
     * @param email
     * @param password
     * @param registrationType
     * @param signature
     */
    public User(String email, String password, String registrationType, String signature, String deviceToken) {
        this.email = email;
        this.password = password;
        this.registrationType = registrationType;
        this.signature = signature;
        this.deviceToken = deviceToken;
        this.deviceType = DEVICE_TYPE_ANDROID;

        System.out.println("email = [" + email + "], password = [" + password + "], registrationType = [" + registrationType + "], signature = [" + signature + "], deviceToken = [" + deviceToken + "]");
    }

    /**
     * Constructor used for register.
     *
     * @param userName
     * @param email
     * @param password
     * @param firstName
     * @param lastName
     * @param signature
     * @param registrationType
     */
    public User(String userName, String email, String password, String firstName, String lastName, String signature, String registrationType, String deviceToken) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.signature = signature;
        this.registrationType = registrationType;
        this.deviceToken = deviceToken;
        this.deviceType = DEVICE_TYPE_ANDROID;
    }

    /**
     * Constructor used for forgot password.
     *
     * @param email
     * @param signature
     */
    public User(String email, String signature) {
        this.email = email;
        this.signature = signature;
    }

    /**
     * Constructor used for setting the categories for a user. After this, the #setSignatuere must be called!
     *
     * @param firstName
     * @param lastName
     * @param categoryIds
     */
    public User(String firstName, String lastName, String categoryIds) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.categoryIds = categoryIds;
    }

    public User(boolean alphabet, String alphabetValue) {
        this.alphabet = alphabet;
        this.alphabetValue = alphabetValue;
    }

    public User(String email, String userId, String firstName, String lastName, String signature, String registrationType, String deviceToken) {
        this.email = email;
        this.userId = userId;
        this.userName = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.signature = signature;
        this.registrationType = registrationType;
        this.deviceToken = deviceToken;
        this.deviceType = DEVICE_TYPE_ANDROID;
    }

    public User(String socialId, String socialProvider, String deviceToken, String signature, int not) {
        this.socialId = socialId;
        this.socialProvider = socialProvider;
        this.deviceToken = deviceToken;
        this.deviceType = DEVICE_TYPE_ANDROID;
        this.signature = signature;
    }

    public User(String userId, String registrationType, String signature, String deviceToken) {
        this.userId = userId;
        this.registrationType = registrationType;
        this.signature = signature;
        this.deviceToken = deviceToken;
        this.deviceType = DEVICE_TYPE_ANDROID;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public int getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(int connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public String getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(String categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<Long> getCategoryIdList() {
        return categoryIdList;
    }

    public void setCategoryIdList(List<Long> categoryIdList) {
        this.categoryIdList = categoryIdList;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public boolean isAlphabet() {
        return alphabet;
    }

    public void setAlphabet(boolean alphabet) {
        this.alphabet = alphabet;
    }

    public String getAlphabetValue() {
        return alphabetValue;
    }

    public void setAlphabetValue(String alphabetValue) {
        this.alphabetValue = alphabetValue;
    }

    public boolean isCheckedForReshare() {
        return checkedForReshare;
    }

    public void setCheckedForReshare(boolean checkedForReshare) {
        this.checkedForReshare = checkedForReshare;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialProvider() {
        return socialProvider;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", apiKey='" + apiKey + '\'' +
                '}';
    }

    public class Account {

        @Expose
        long id;

        @Expose
        String provider;

        @Expose
        @SerializedName("social_id")
        String socialId;

        public long getId() {
            return id;
        }

        public String getProvider() {
            return provider;
        }

        public String getSocialId() {
            return socialId;
        }
    }
}
