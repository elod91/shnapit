package shnapit.shnapit.database.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;
import java.util.List;

import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.rest.responses.Category;

@Table(database = ShnapitDatabase.class)
public class Product extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = false)
    @Expose
    long id;

    @Column
    @Expose
    String brand;

    @Column
    @Expose
    String name;

    @Column
    @Expose
    @SerializedName("link")
    String website;

    @Column
    @Expose
    @SerializedName("photo")
    String photoUrl;

    @Expose
    @SerializedName("categories")
    List<Category> category;

    @Column
    @Expose
    @SerializedName("created_at")
    Date createdAt;

    @Column
    @Expose
    @SerializedName("updated_at")
    Date updateAt;

    @Column
    @Expose
    @SerializedName("last_modified")
    Date lastModified;

    @Column
    @Expose
    @SerializedName("date_order")
    Date dateOrder;

    @Expose
    @SerializedName("created_user")
    User author;

    @Expose
    @SerializedName("last_updated_user")
    User updatedBy;

    @Expose
    @SerializedName("share_with_user")
    User shareWith;

    @Expose
    List<String> keywords;

    @Column
    @Expose
    @SerializedName("wanted_by_users_count")
    int wantedByCount;

    @Column
    @Expose
    @SerializedName("seen")
    int seenByCounter;

    @Column
    @Expose
    String price;

    @Column
    @Expose
    @SerializedName("is_locked")
    boolean locked;

    @Column
    @Expose
    boolean wanted;

    @Column
    String categoryName;

    @Column
    long categoryId;

    @Column
    @Expose
    @SerializedName("user_id")
    long authorId;

    @Column
    @Expose
    @SerializedName("last_updated_user_id")
    long updatedById;

    @Column
    long shareWithId;

    @Column
    String keywordsJoint;

    boolean actionsExpanded;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Date getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(Date dateOrder) {
        this.dateOrder = dateOrder;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getShareWith() {
        return shareWith;
    }

    public void setShareWith(User shareWith) {
        this.shareWith = shareWith;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public int getWantedByCount() {
        return wantedByCount;
    }

    public void setWantedByCount(int wantedByCount) {
        this.wantedByCount = wantedByCount;
    }

    public int getSeenByCounter() {
        return seenByCounter;
    }

    public void setSeenByCounter(int seenByCounter) {
        this.seenByCounter = seenByCounter;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public boolean isWanted() {
        return wanted;
    }

    public void setWanted(boolean wanted) {
        this.wanted = wanted;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public long getUpdatedById() {
        return updatedById;
    }

    public void setUpdatedById(long updatedById) {
        this.updatedById = updatedById;
    }

    public long getShareWithId() {
        return shareWithId;
    }

    public void setShareWithId(long shareWithId) {
        this.shareWithId = shareWithId;
    }

    public String getKeywordsJoint() {
        return keywordsJoint;
    }

    public void setKeywordsJoint(String keywordsJoint) {
        this.keywordsJoint = keywordsJoint;
    }

    public boolean isActionsExpanded() {
        return actionsExpanded;
    }

    public void setActionsExpanded(boolean actionsExpanded) {
        this.actionsExpanded = actionsExpanded;
    }
}
