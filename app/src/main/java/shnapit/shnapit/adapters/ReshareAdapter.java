package shnapit.shnapit.adapters;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import shnapit.shnapit.R;
import shnapit.shnapit.database.models.User;

public class ReshareAdapter extends RecyclerView.Adapter<ReshareAdapter.ViewHolder> {

    private List<User> connections;

    public ReshareAdapter(List<User> connections) {
        this.connections = connections;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reshare, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.check.setChecked(connections.get(position).isCheckedForReshare());
        holder.name.setText(String.format("%s %s", connections.get(position).getFirstName(), connections.get(position).getLastName()));

        holder.image.setImageResource(R.drawable.ic_user_default);
        if (connections.get(position).getProfileImage() != null && !connections.get(position).getProfileImage().isEmpty())
            Picasso.with(holder.itemView.getContext()).load(connections.get(position).getProfileImage()).into(holder.image);

        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                connections.get(position).setCheckedForReshare(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return connections.size();
    }

    public List<Long> getSelectedUserIds() {
        List<Long> selectedUserIds = new ArrayList<>();
        for (User connection : connections)
            if (connection.isCheckedForReshare())
                selectedUserIds.add(connection.getId());

        return selectedUserIds;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private AppCompatCheckBox check;
        private ImageView image;
        private TextView name;

        public ViewHolder(View itemView) {
            super(itemView);

            check = (AppCompatCheckBox) itemView.findViewById(R.id.list_item_reshare_check);
            image = (ImageView) itemView.findViewById(R.id.list_item_reshare_image);
            name = (TextView) itemView.findViewById(R.id.list_item_reshare_name);
        }
    }
}
