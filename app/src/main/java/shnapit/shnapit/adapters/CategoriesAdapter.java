package shnapit.shnapit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import shnapit.shnapit.R;
import shnapit.shnapit.activities.CategoriesActivity;
import shnapit.shnapit.rest.responses.Category;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private List<Category> data;

    private int cameFrom;

    public CategoriesAdapter(Context context, int cameFrom, List<Category> categories) {
        this.cameFrom = cameFrom;
        this.data = categories;

        if (this.cameFrom == CategoriesActivity.FROM_HOME)
            this.data.add(0, new Category(-1, context.getString(R.string.category_all), false));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.categoryName.setText(data.get(position).getName());
        holder.categoryName.setSelected(data.get(position).isSelected());
        holder.categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private void categoryClicked(int position) {
        switch (cameFrom) {
            case CategoriesActivity.FROM_EDIT:
                for (Category category : data)
                    category.setSelected(false);

                data.get(position).setSelected(true);
                notifyDataSetChanged();
                break;
            case CategoriesActivity.FROM_HOME:
                if (position == 0) {
                    for (Category category : data)
                        category.setSelected(false);
                    data.get(position).setSelected(true);
                    notifyDataSetChanged();
                    return;
                }

                data.get(0).setSelected(false);
                data.get(position).setSelected(!data.get(position).isSelected());
                notifyDataSetChanged();
        }
    }

    public List<Category> getData() {
        return data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView categoryName;

        public ViewHolder(View itemView) {
            super(itemView);

            categoryName = (TextView) itemView.findViewById(R.id.list_item_category_name);
        }
    }
}

