package shnapit.shnapit.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.activities.EditProductActivity;
import shnapit.shnapit.activities.ProductWebsiteActivity;
import shnapit.shnapit.activities.ReshareProductActivity;
import shnapit.shnapit.database.models.Product;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.services.ProductService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.TagFormat;
import shnapit.shnapit.utils.ViewUtils;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    public static final int DOUBLE_PRESS_INTERVAL = 200;

    private List<Product> data;
    private Activity activity;

    private ProductService productService;
    private String apiToken;

    private boolean showDropdown;

    private OnProductRefreshListener onProductRefreshListener;

    public ProductAdapter(Activity activity, OnProductRefreshListener onProductRefreshListener) {
        this.data = new ArrayList<>();
        this.onProductRefreshListener = onProductRefreshListener;
        this.activity = activity;
        this.apiToken = PersistentUtils.getString(activity, PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);
        this.productService = RestClient.getInstance().getRetrofit().create(ProductService.class);
    }

    public void setData(List<Product> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void addMultipleData(List<Product> products) {
        data.addAll(products);
        notifyDataSetChanged();
    }

    public void removeData() {
        data.clear();
        notifyDataSetChanged();
    }

    public void setShowDropdown(boolean showDropdown) {
        this.showDropdown = showDropdown;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.itemView.setPadding(0, 0, 0, 0);
        if (position == 0) {
            holder.itemView.setPadding(0, (int) holder.itemView.getContext().getResources().getDimension(R.dimen.saved_search_bar_height), 0, 0);
            showInitialDropdown(holder);
        }

        Picasso.with(holder.itemView.getContext()).load(data.get(position).getPhotoUrl()).into(holder.productImage);
        setImageClickListener(data.get(position), holder);

        holder.brandAndName.setText(String.format("%s - %s", data.get(position).getBrand(), data.get(position).getName()));
        holder.price.setText(data.get(position).getPrice());
        holder.category.setText(data.get(position).getCategoryName());
        holder.seenByCounter.setText(String.format("%d", data.get(position).getSeenByCounter()));
        holder.wantedCounter.setText(String.format("%d", data.get(position).getWantedByCount()));
        holder.timeAgo.setText(DateUtils.getRelativeTimeSpanString(data.get(position).getDateOrder().getTime(), System.currentTimeMillis(), 0L, DateUtils.FORMAT_ABBREV_ALL));
        holder.wantOrRemove.setText(data.get(position).isWanted() ? holder.itemView.getContext().getString(R.string.product_remove) : holder.itemView.getContext().getString(R.string.product_want));

        holder.actionsContainer.setVisibility(View.GONE);
        holder.expandCollapseActions.setImageResource(R.drawable.ic_actions_expand);
        if (data.get(position).isActionsExpanded()) {
            holder.actionsContainer.setVisibility(View.VISIBLE);
            holder.expandCollapseActions.setImageResource(R.drawable.ic_actions_collapse);
        }

        holder.expandCollapseActions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandCollapseActions(data.get(position), holder);
            }
        });

        holder.wantRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wantOrRemove(data.get(position), holder);
            }
        });

        holder.reshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reshare(data.get(position));
            }
        });
        holder.buy.setVisibility(View.VISIBLE);
        holder.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy(data.get(position));
            }
        });
        if (data.get(position).getWebsite() == null || data.get(position).getWebsite().isEmpty()) {
            holder.buy.setVisibility(View.GONE);
            holder.buy.setOnClickListener(null);
        }
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProduct(data.get(position));
            }
        });
        holder.report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                report(position);
            }
        });

        if (data.get(position).getUpdatedBy() != null) {
            holder.authorImage.setImageResource(R.drawable.ic_user_default);
            if (data.get(position).getUpdatedBy().getProfileImage() != null && !data.get(position).getUpdatedBy().getProfileImage().isEmpty())
                Picasso.with(holder.itemView.getContext()).load(data.get(position).getUpdatedBy().getProfileImage()).into(holder.authorImage);
            holder.authorName.setText(String.format("%s %s", data.get(position).getUpdatedBy().getFirstName(), data.get(position).getUpdatedBy().getLastName()));
            return;
        }

        holder.authorName.setText(R.string.product_default_author);
        holder.authorImage.setImageResource(R.drawable.ic_discovery_toolbar);
    }

    private void setImageClickListener(final Product product, final ViewHolder holder) {
        holder.productImage.setOnClickListener(new View.OnClickListener() {
            boolean hasDoubleClicked;
            long lastPressTime;

            @Override
            public void onClick(View v) {
                checkDoubleClick();
//                if (hasDoubleClicked)
//                    editProduct(product);
            }

            private boolean checkDoubleClick() {
                long pressTime = System.currentTimeMillis();
                if (pressTime - lastPressTime <= DOUBLE_PRESS_INTERVAL) {
                    hasDoubleClicked = true;
                    lastPressTime = 0;
                    lastPressTime = pressTime;
                    return true;
                }

                hasDoubleClicked = false;
                Handler myHandler = new Handler() {
                    public void handleMessage(Message m) {

                        if (!hasDoubleClicked) {
                            lastPressTime = 0;
                            onSingleTapImage(product, holder);
                        }
                    }
                };
                Message m = new Message();
                myHandler.sendMessageDelayed(m, DOUBLE_PRESS_INTERVAL);

                lastPressTime = pressTime;
                return hasDoubleClicked;
            }
        });
    }

    private void showInitialDropdown(final ViewHolder holder) {
        if (showDropdown) {
            PersistentUtils.setBoolean(holder.itemView.getContext(), false, PersistentUtils.SHOW_DROPDOWN);
            showDropdown = false;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.expandCollapseActions.setImageResource(R.drawable.ic_actions_collapse);
                    ViewUtils.expand(holder.actionsContainer, new ViewUtils.ExpandCollapseAnimationListener() {
                        @Override
                        public void onAnimationEnd() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    holder.expandCollapseActions.setImageResource(R.drawable.ic_actions_expand);
                                    ViewUtils.collapse(holder.actionsContainer);
                                }
                            }, 500);
                        }
                    });
                }
            }, 200);
        }
    }

    private void onSingleTapImage(Product product, ViewHolder holder) {
//        if (!product.isWanted())
//            wantOrRemove(product, holder);

        if (product.getWebsite() != null && !product.getWebsite().isEmpty())
            buy(product);
    }

    private void expandCollapseActions(Product product, ViewHolder holder) {
        if (product.isActionsExpanded()) {
            product.setActionsExpanded(false);
            holder.expandCollapseActions.setImageResource(R.drawable.ic_actions_expand);
            ViewUtils.collapse(holder.actionsContainer);
            return;
        }

        product.setActionsExpanded(true);
        holder.expandCollapseActions.setImageResource(R.drawable.ic_actions_collapse);
        ViewUtils.expand(holder.actionsContainer);
    }

    private void wantOrRemove(final Product product, final ViewHolder holder) {
        if (!RestClient.hasNetworkConnection(activity, true))
            return;

        productService.wantProduct(apiToken, product.getId(), !product.isWanted(), RestClient.getSignature(activity, apiToken, TagFormat.from(RestConstants.PRODUCT_WANT).with("api_token", apiToken).with("id", product.getId()).format())).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Response<Object> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(activity, response.errorBody());
                    return;
                }

                product.setWanted(!product.isWanted());
                if (product.isWanted())
                    product.setWantedByCount(product.getWantedByCount() + 1);
                else
                    product.setWantedByCount(product.getWantedByCount() - 1);
                holder.wantOrRemove.setText(product.isWanted() ? activity.getString(R.string.product_remove) : activity.getString(R.string.product_want));
                holder.wantedCounter.setText(String.format("%d", product.getWantedByCount()));
            }

            @Override
            public void onFailure(Throwable t) {
                DialogFactory.showDialogOneButton(activity, activity.getString(R.string.dialog_error_title), activity.getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void reshare(Product product) {
        onProductRefreshListener.doRefresh();

        Intent intent = new Intent(activity, ReshareProductActivity.class);
        intent.putExtra(ReshareProductActivity.RESHARE_PRODUCT_ID, product.getId());

        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);
    }

    private void buy(Product product) {
        if (!RestClient.hasNetworkConnection(activity, true))
            return;

        Intent intent = new Intent(activity, ProductWebsiteActivity.class);
        intent.putExtra(ProductWebsiteActivity.PRODUCT_WEBSITE, product.getWebsite());
        intent.putExtra(ProductWebsiteActivity.PRODUCT_NAME, product.getName());

        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);
    }

    private void editProduct(Product product) {
        if (!RestClient.hasNetworkConnection(activity, true))
            return;

        Intent intent = new Intent(activity, EditProductActivity.class);
        intent.putExtra(EditProductActivity.IMAGE_PATH, product.getPhotoUrl());
        intent.putExtra(EditProductActivity.OPERATION, EditProductActivity.OPERATION_EDIT);
        intent.putExtra(EditProductActivity.PRODUCT_ID, product.getId());

        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.in_from_right_acitivty, R.anim.zoom_out);
    }

    private void report(final int position) {
        if (!RestClient.hasNetworkConnection(activity, true))
            return;

        DialogFactory.showDialog(activity, activity.getString(R.string.dialog_remove_product_title), activity.getString(R.string.dialog_remove_product_message), activity.getString(R.string.dialog_remove_product_positive), activity.getString(R.string.dialog_negative_btn), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                productService.removeProduct(apiToken, data.get(position).getId(), RestClient.getSignature(activity, apiToken, TagFormat.from(RestConstants.PRODUCT_REMOVE).with("api_token", apiToken).with("id", data.get(position).getId()).format())).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Response<Object> response) {
                        if (response.body() == null) {
                            RestClient.handleErrors(activity, response.errorBody());
                            return;
                        }

                        data.remove(position);
                        notifyItemRemoved(position);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        DialogFactory.showDialogOneButton(activity, activity.getString(R.string.dialog_error_title), activity.getString(R.string.dialog_general_error_message), null);
                    }
                });
            }

            @Override
            public void onNegative() {

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnProductRefreshListener {
        void doRefresh();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout actionsContainer;
        public LinearLayout wantRemove, reshare, buy, edit, report;
        public ImageView productImage, expandCollapseActions;
        public CircleImageView authorImage;
        public TextView brandAndName, price, category, seenByCounter, authorName, wantedCounter, timeAgo, wantOrRemove;

        public ViewHolder(View itemView) {
            super(itemView);
            actionsContainer = (RelativeLayout) itemView.findViewById(R.id.product_actions);

            wantRemove = (LinearLayout) itemView.findViewById(R.id.product_want_remove);
            reshare = (LinearLayout) itemView.findViewById(R.id.product_reshare);
            buy = (LinearLayout) itemView.findViewById(R.id.product_buy);
            edit = (LinearLayout) itemView.findViewById(R.id.product_edit);
            report = (LinearLayout) itemView.findViewById(R.id.product_report);

            productImage = (ImageView) itemView.findViewById(R.id.product_image);
            expandCollapseActions = (ImageView) itemView.findViewById(R.id.product_expand_collapse_actions);
            authorImage = (CircleImageView) itemView.findViewById(R.id.author_image);

            brandAndName = (TextView) itemView.findViewById(R.id.brand_and_name);
            price = (TextView) itemView.findViewById(R.id.price);
            category = (TextView) itemView.findViewById(R.id.category);
            seenByCounter = (TextView) itemView.findViewById(R.id.seen_by_counter);
            authorName = (TextView) itemView.findViewById(R.id.author_name);
            wantedCounter = (TextView) itemView.findViewById(R.id.wanted_by_counter);
            timeAgo = (TextView) itemView.findViewById(R.id.product_time_ago);
            wantOrRemove = (TextView) itemView.findViewById(R.id.product_want_remove_text);
        }
    }
}
