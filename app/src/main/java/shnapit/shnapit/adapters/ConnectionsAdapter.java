package shnapit.shnapit.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lb.recyclerview_fast_scroller.RecyclerViewFastScroller;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import shnapit.shnapit.R;
import shnapit.shnapit.database.ShnapitDatabase;
import shnapit.shnapit.database.managers.UserDbManager;
import shnapit.shnapit.database.models.User;
import shnapit.shnapit.fragments.MyConnectionsFragment;
import shnapit.shnapit.rest.RestClient;
import shnapit.shnapit.rest.RestConstants;
import shnapit.shnapit.rest.services.ConnectionService;
import shnapit.shnapit.utils.DialogFactory;
import shnapit.shnapit.utils.PersistentUtils;
import shnapit.shnapit.utils.TagFormat;

public class ConnectionsAdapter extends RecyclerView.Adapter<ConnectionsAdapter.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter {

    private Context context;
    private ConnectionService connectionService;
    private String apiToken;

    private List<User> data;

    private OnConnectionsChangedListener onConnectionsChangedListener;
    private Fragment parent;

    public ConnectionsAdapter(Context context, Fragment parent) {
        this.context = context;
        this.parent = parent;
        this.connectionService = RestClient.getInstance().getRetrofit().create(ConnectionService.class);
        this.apiToken = PersistentUtils.getString(context, PersistentUtils.LOGGED_IN_USER_API_TOKEN, null);
    }

    public void setData(List<User> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(OnConnectionsChangedListener onConnectionsChangedListener) {
        this.onConnectionsChangedListener = onConnectionsChangedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_connection, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (data.get(position).isAlphabet()) {
            holder.connectionContainer.setVisibility(View.GONE);

            holder.alphabet.setVisibility(View.VISIBLE);
            holder.alphabet.setText(data.get(position).getAlphabetValue());
            return;
        }

        holder.alphabet.setVisibility(View.GONE);
        holder.connectionContainer.setVisibility(View.VISIBLE);

        holder.name.setText(String.format("%s %s", data.get(position).getFirstName(), data.get(position).getLastName()));

        switch (data.get(position).getConnectionStatus()) {
            case User.CONNECTION_STATUS_ACCEPTED:
                holder.connectionStatus.setImageResource(R.drawable.ic_connection);
                break;

            case User.CONNECTION_STATUS_PENDING:
                holder.connectionStatus.setImageResource(R.drawable.ic_pending_connection);
                break;

            case User.CONNECTION_STATUS_PENDING_REQUESTS:
                holder.connectionStatus.setImageResource(R.drawable.ic_add_to_connections);
        }
        holder.connectionStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onActionClicked(position);
            }
        });

        holder.profilePhoto.setImageResource(R.drawable.ic_user_default);
        if (data.get(position).getProfileImage() != null && !data.get(position).getProfileImage().isEmpty())
            Picasso.with(context).load(data.get(position).getProfileImage()).into(holder.profilePhoto);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public String getTextToShowInBubble(int pos) {
        if (data.get(pos).isAlphabet())
            return data.get(pos).getAlphabetValue();
        return Character.toString(data.get(pos).getFirstName().charAt(0)).toUpperCase();
    }

    private void onActionClicked(int position) {
        if (!RestClient.hasNetworkConnection(context, true))
            return;

        switch (data.get(position).getConnectionStatus()) {
            case User.CONNECTION_STATUS_ACCEPTED:
                unfriendUser(position);
                break;

            case User.CONNECTION_STATUS_PENDING:
                unfriendUser(position);
                break;

            case User.CONNECTION_STATUS_PENDING_REQUESTS:
                if (parent instanceof MyConnectionsFragment) {
                    createConnection(position);
                    return;
                }

                acceptRequest(position);
        }
    }

    private void unfriendUser(final int position) {
        final User connection = data.get(position);

        DialogFactory.showDialog(context, context.getString(R.string.dialog_confirm_title), TagFormat.from(context.getString(R.string.dialog_un_friend_message)).with("username", String.format("%s %s", connection.getFirstName(), connection.getLastName())).format(), context.getString(R.string.dialog_yes), context.getString(R.string.dialog_no), new DialogFactory.OnActionSelectedDialog() {
            @Override
            public void onPositive() {
                connectionService.deleteConnection(apiToken, "email", connection.getUserName(), RestClient.getSignature(context, apiToken, TagFormat.from(RestConstants.DELETE_CONNECTION).with("api_token", apiToken).format())).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Response<Object> response) {
                        if (response.body() == null) {
                            RestClient.handleErrors(context, response.errorBody());
                            return;
                        }

                        UserDbManager.getInstance().deleteUser(connection.getId(), new ShnapitDatabase.OnResultReceivedListener<Void>() {
                            @Override
                            public void onResultReceived(Void result) {
                                onConnectionsChangedListener.onConnectionChanged();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        DialogFactory.showDialogOneButton(context, context.getString(R.string.dialog_error_title), context.getString(R.string.dialog_general_error_message), null);
                    }
                });
            }

            @Override
            public void onNegative() {

            }
        });
    }

    private void createConnection(int position) {
        User connection = data.get(position);
        connectionService.createConnection(apiToken, "email", connection.getUserName(), RestClient.getSignature(context, apiToken, TagFormat.from(RestConstants.CONNECTIONS_GENERAL).with("api_token", apiToken).format())).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Response<Object> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(context, response.errorBody());
                    return;
                }

                onConnectionsChangedListener.onConnectionChanged();
            }

            @Override
            public void onFailure(Throwable t) {
                DialogFactory.showDialogOneButton(context, context.getString(R.string.dialog_error_title), context.getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    private void acceptRequest(int position) {
        User connection = data.get(position);
        connectionService.acceptRequest(apiToken, "email", connection.getUserName(), RestClient.getSignature(context, apiToken, TagFormat.from(RestConstants.ACCEPT_REQUEST).with("api_token", apiToken).format())).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Response<Object> response) {
                if (response.body() == null) {
                    RestClient.handleErrors(context, response.errorBody());
                    return;
                }

                onConnectionsChangedListener.onConnectionChanged();
            }

            @Override
            public void onFailure(Throwable t) {
                DialogFactory.showDialogOneButton(context, context.getString(R.string.dialog_error_title), context.getString(R.string.dialog_general_error_message), null);
            }
        });
    }

    public interface OnConnectionsChangedListener {
        void onConnectionChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout connectionContainer;

        public TextView name;
        public ImageView profilePhoto, connectionStatus;

        public TextView alphabet;

        public ViewHolder(View itemView) {
            super(itemView);
            connectionContainer = (RelativeLayout) itemView.findViewById(R.id.list_item_connection_container);

            name = (TextView) itemView.findViewById(R.id.list_item_connection_name);

            profilePhoto = (ImageView) itemView.findViewById(R.id.list_item_connection_photo);
            connectionStatus = (ImageView) itemView.findViewById(R.id.list_item_connection_action);

            alphabet = (TextView) itemView.findViewById(R.id.list_item_connection_alphabet);
        }
    }
}
