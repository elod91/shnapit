package shnapit.shnapit.gcm;


import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import shnapit.shnapit.R;


public class RegistrationIntentService extends IntentService {
    public static final String TOKEN = "gcm-token";
    public static final String TOKEN_ACTION = "net.reea.meeting.GetToken";
    private static final String TAG = RegistrationIntentService.class.getSimpleName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Intent broadcastIntent = new Intent(TOKEN_ACTION);
            broadcastIntent.putExtra(TOKEN, token);
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        } catch (Exception e) {
            Log.e(TAG, "Failed to complete token refresh", e);
        }
    }
}
