package shnapit.shnapit.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

import shnapit.shnapit.R;
import shnapit.shnapit.activities.HomeActivity;
import shnapit.shnapit.activities.SplashActivity;
import shnapit.shnapit.application.ShnapitApplication;

public class ShnapitGcmListenerService extends GcmListenerService {
    public static final String GCM_TYPE = "type";
    public static final String GCM_MESSAGE = "message";

    public static final String GCM_TYPE_NEW_FRIEND_REQUEST = "friend_request";
    public static final String GCM_TYPE_FRIEND_DELETED = "friend_delete";
    public static final String GCM_TYPE_NEW_SHARE = "feed";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        System.out.println("from = [" + from + "], data = [" + data.get(GCM_TYPE) + " ----- " + data.get(GCM_MESSAGE) + "]");

        String gcmType = data.getString(GCM_TYPE);
        if (gcmType == null) return;

        if (gcmType.equalsIgnoreCase(GCM_TYPE_NEW_FRIEND_REQUEST)) {
            if (!ShnapitApplication.isApplicationRunning()) {
                sendNotification(data.getString(GCM_MESSAGE));
                return;
            }
            sendBroadcast(new Intent(HomeActivity.ACTION_FRIEND_REQUEST));
            return;
        }

        if (gcmType.equalsIgnoreCase(GCM_TYPE_FRIEND_DELETED)) {
            sendBroadcast(new Intent(HomeActivity.ACTION_FRIEND_REQUEST));
            return;
        }

        if (gcmType.equalsIgnoreCase(GCM_TYPE_NEW_SHARE)) {
            String message = data.getString(GCM_MESSAGE);
            if (!ShnapitApplication.isApplicationRunning()) {
                sendNotification(message);
                return;
            }

            Intent newShare = new Intent(HomeActivity.ACTION_NEW_SHARE);
            newShare.putExtra(HomeActivity.MESSAGE_NEW_SHARE, message);

            sendBroadcast(newShare);
        }
    }

    private void sendNotification(String message) {
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, SplashActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        notificationManager.notify(0, new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .build());
    }

}

