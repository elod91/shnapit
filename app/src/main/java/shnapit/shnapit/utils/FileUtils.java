package shnapit.shnapit.utils;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import shnapit.shnapit.application.ShnapitApplication;

public class FileUtils {
    public static final String DEFAULT_IMAGE_FORMAT = "png";

    private static FileUtils INSTANCE;

    private FileUtils() {
    }

    public static synchronized FileUtils getInstance() {
        if (INSTANCE == null)
            INSTANCE = new FileUtils();

        return INSTANCE;
    }

    public void saveBitmapToFile(String filePath, Bitmap bitmap) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(filePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null)
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public String getNewEditedProductFilePath() {
        String filePath = Environment.getExternalStorageDirectory() + ShnapitApplication.SAVE_FOLDER;

        File saveFolder = new File(filePath);
        if (!saveFolder.exists())
            saveFolder.mkdir();

        return String.format("%s/%d.%s", filePath, System.currentTimeMillis(), DEFAULT_IMAGE_FORMAT);
    }
}
