package shnapit.shnapit.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PersistentUtils {
    public static final String LOGGED_IN_USER_ID = "logged-in-user-id";
    public static final String LOGGED_IN_USER_API_TOKEN = "logged-in-user-api-token";
    public static final String SERVER_TIMESTAMP = "server-timestamp";
    public static final String DEVICE_TIMESTAMP = "device-timestamp";
    public static final String DEVICE_TOKEN = "device-token";
    public static final String SHOW_DROPDOWN = "dropdown-shown";
    public static final String LOGGED_IN_WITH_FACEBOOK = "logged-in-with-facebook";

    public static final int NO_USER_ID = -1;

    public static SharedPreferences obtainSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean clearSharedPreferences(Context context) {
        SharedPreferences.Editor editor = obtainSharedPreferences(context).edit();
        editor.clear();
        return editor.commit();
    }

    public static boolean setString(Context context, String strValue, String prefKey) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
        e.putString(prefKey, strValue);
        return e.commit();
    }

    public static boolean setBoolean(Context context, boolean value, String prefKey) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
        e.putBoolean(prefKey, value);
        return e.commit();
    }

    public static boolean setInt(Context context, int value, String prefKey) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
        e.putInt(prefKey, value);
        return e.commit();
    }

    public static boolean setLong(Context context, long value, String prefKey) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
        e.putLong(prefKey, value);
        return e.commit();
    }

    public static String getString(Context context, String prefKey, String defValue) {
        return obtainSharedPreferences(context).getString(prefKey, defValue);
    }

    public static boolean getBoolean(Context context, String prefKey, boolean defValue) {
        return obtainSharedPreferences(context).getBoolean(prefKey, defValue);
    }

    public static int getInt(Context context, String prefKey, int defValue) {
        return obtainSharedPreferences(context).getInt(prefKey, defValue);
    }

    public static long getLong(Context context, String prefKey, long defValue) {
        return obtainSharedPreferences(context).getLong(prefKey, defValue);
    }
}
