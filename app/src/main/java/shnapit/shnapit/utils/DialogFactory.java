package shnapit.shnapit.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.view.WindowManager;

import shnapit.shnapit.R;
import shnapit.shnapit.views.ColorPickerDialog;
import shnapit.shnapit.views.SwipeActionDialog;

public class DialogFactory {

    private static ColorPickerDialog colorPickerDialog;
    private static SwipeActionDialog swipeActionDialog;

    /**
     * Shows a dialog with a custom title and message and a single OK button.
     *
     * @param context
     * @param title                   the title of the dialog
     * @param message                 the content message of the dialog
     * @param onButtonPressedListener dialog button callback
     */
    public static void showDialogOneButton(Context context, String title, String message, final OnButtonPressedListener onButtonPressedListener) {
        new AlertDialog
                .Builder(context, R.style.AlertDialog_AppCompat_Light_Shnapit)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_positive_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (onButtonPressedListener != null)
                            onButtonPressedListener.onButtonPressed();
                    }
                })
                .setCancelable(false)
                .show();
    }

    /**
     * Shows a dialog with a custom title, message, positive and negative buttons.
     *
     * @param context
     * @param title                  the title of the dialog
     * @param message                the content message of the dialog
     * @param positiveButton         the positive button's text
     * @param negativeButton         the negative button's text
     * @param onActionSelectedDialog dialog button callback
     */
    public static void showDialog(Context context, String title, String message, String positiveButton, String negativeButton, final OnActionSelectedDialog onActionSelectedDialog) {
        new AlertDialog
                .Builder(context, R.style.AlertDialog_AppCompat_Light_Shnapit)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButton == null ? context.getString(R.string.dialog_positive_btn) : positiveButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (onActionSelectedDialog != null)
                            onActionSelectedDialog.onPositive();
                    }
                })
                .setNegativeButton(negativeButton == null ? context.getString(R.string.dialog_negative_btn) : negativeButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (onActionSelectedDialog != null)
                            onActionSelectedDialog.onNegative();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public static void showPhotoChooserDialog(Context context, final OnPhotoChooserListener onPhotoChooserListener) {
        new AlertDialog.Builder(context, R.style.AlertDialog_AppCompat_Light_Shnapit)
                .setTitle(R.string.dialog_photo_chooser_title)
                .setItems(R.array.dialog_photo_choose_options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (onPhotoChooserListener != null)
                            onPhotoChooserListener.onActionSelected(which);
                    }
                })
                .create()
                .show();
    }

    public static void showColorPickerDialog(Context context, ColorPickerDialog.OnColorChangedListener onColorChangedListener) {
        if (colorPickerDialog == null || !colorPickerDialog.getContext().equals(context)) {
            colorPickerDialog = new ColorPickerDialog(context, onColorChangedListener);
            colorPickerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            colorPickerDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            colorPickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            colorPickerDialog.getWindow().setWindowAnimations(R.style.SlideInAndOut);
        }

        colorPickerDialog.show();
    }

    public static void hideColorPickerDialog() {
        if (colorPickerDialog != null)
            colorPickerDialog.dismiss();
    }

    /**
     * Shows a dialog with a custom title and message and predefined positive and negative button texts.
     *
     * @param context
     * @param title                  the title of the dialog
     * @param message                the content message of the dialog
     * @param onActionSelectedDialog dialog button callback
     */
    public static void showDialog(Context context, String title, String message, final OnActionSelectedDialog onActionSelectedDialog) {
        showDialog(context, title, message, null, null, onActionSelectedDialog);
    }

    public static void showSwipeActionDialog(Context context, String text, int imageRes) {
        if (swipeActionDialog == null || !swipeActionDialog.getContext().equals(context))
            swipeActionDialog = new SwipeActionDialog(context);

        swipeActionDialog.setTextAndImage(text, imageRes);
        swipeActionDialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeActionDialog.dismiss();
            }
        }, 750);
    }

    public interface OnActionSelectedDialog {
        void onPositive();

        void onNegative();
    }

    public interface OnButtonPressedListener {
        void onButtonPressed();
    }

    public interface OnPhotoChooserListener {
        int ACTION_CAMERA = 0;
        int ACTION_GALLERY = 1;

        void onActionSelected(int action);
    }
}
