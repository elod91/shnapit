package shnapit.shnapit.utils;

import android.app.ProgressDialog;
import android.content.Context;

import shnapit.shnapit.R;

public class ProgressFactory {
    public static final String DEFAULT_TITLE = "Working";
    public static final String DEFAULT_MESSAGE = "Please wait...";

    private static ProgressDialog progressDialog;

    public static void showProgressDialog(Context context, String title, String message) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new ProgressDialog(context, R.style.ProgressBar);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void showProgressDialog(Context context) {
        showProgressDialog(context, DEFAULT_TITLE, DEFAULT_MESSAGE);
    }

    public static void hideProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
