package shnapit.shnapit.utils;


import android.graphics.Bitmap;
import android.os.AsyncTask;

public class SaveBitmapToFile extends AsyncTask<Void, Void, Void> {

    private String filePath;
    private Bitmap imageBitmap;
    private OnSaveFinishedListener onSaveFinishedListener;

    public SaveBitmapToFile(String filePath, Bitmap imageBitmap, OnSaveFinishedListener onSaveFinishedListener) {
        this.filePath = filePath;
        this.imageBitmap = imageBitmap;
        this.onSaveFinishedListener = onSaveFinishedListener;
    }

    @Override
    protected Void doInBackground(Void... params) {
        FileUtils.getInstance().saveBitmapToFile(filePath, imageBitmap);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        onSaveFinishedListener.onSaveFinished();
    }

    public interface OnSaveFinishedListener {
        void onSaveFinished();
    }
}
