package shnapit.shnapit.utils;

import java.util.Comparator;

import shnapit.shnapit.database.models.User;

public class UserComparator implements Comparator<User> {

    private static UserComparator INSTANCE;

    public static UserComparator getInstance() {
        if (INSTANCE == null)
            INSTANCE = new UserComparator();

        return INSTANCE;
    }

    @Override
    public int compare(User lhs, User rhs) {
        return lhs.getFirstName().toLowerCase().compareTo(rhs.getFirstName().toLowerCase());
    }
}
