package shnapit.shnapit.utils;

import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import shnapit.shnapit.rest.RestConstants;

public class SignatureUtil {

    public static String getSignature(String apiToken, String endPoint, long timestamp) {
        try {
            return getMd5Hash(apiToken + "_" + endPoint + "_" + getFormattedTimestamp(timestamp));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getSignature(String apiToken, String endPoint) {
        try {
            return getMd5Hash(apiToken + "_" + endPoint + "_" + RestConstants.UPLOAD_HASH_KEY);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String getFormattedTimestamp(Long timestamp) {
        return (timestamp - (timestamp % 600)) + "";
    }

    private static String getMd5Hash(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String md5 = number.toString(16);

            while (md5.length() < 32) {
                md5 = "0" + md5;
            }

            return md5;
        } catch (NoSuchAlgorithmException e) {
            Log.e("MD5", e.getMessage());
            return null;
        }
    }

}
