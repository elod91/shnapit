package shnapit.shnapit.utils;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

public class GeneralUtil {

    public static void hideSoftKeyboard(Context ctx) {
        if (ctx == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) ctx.getSystemService(Activity.INPUT_METHOD_SERVICE);

        if (inputMethodManager != null && ((Activity) ctx).getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(((Activity) ctx).getCurrentFocus().getWindowToken(), 0);
        }
    }
}
