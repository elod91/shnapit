package shnapit.shnapit.utils;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class SavedListRecyclerView extends RecyclerView {

    private HideShowSearchBarListener searchBarListener;

    private boolean isStartYSet;
    private float startY;
    private int displayHeight;

    public SavedListRecyclerView(Context context) {
        super(context);
    }

    public SavedListRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SavedListRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (displayHeight == 0)
            return super.onTouchEvent(event);

        switch (MotionEventCompat.getActionMasked(event)) {
            case (MotionEvent.ACTION_MOVE):
                if (!isStartYSet) {
                    startY = event.getY();
                    isStartYSet = true;
                }

                int difference = (int) (startY - event.getY());
                if (difference < 0) {
                    if (displayHeight / 100 < -difference) {
                        searchBarListener.showSearchBar();
                        isStartYSet = false;
                        return super.onTouchEvent(event);
                    }
                    return super.onTouchEvent(event);
                }

                if (displayHeight / 100 < difference) {
                    searchBarListener.hideSearchBar();
                    isStartYSet = false;
                    return super.onTouchEvent(event);
                }

                return super.onTouchEvent(event);
            case (MotionEvent.ACTION_UP):
                isStartYSet = false;
                return super.onTouchEvent(event);
            default:
                return super.onTouchEvent(event);
        }
    }

    public void setHideShowSearchBarListener(HideShowSearchBarListener hideShowSearchBarListener) {
        searchBarListener = hideShowSearchBarListener;
    }

    public void setDisplayHeight(int displayHeight) {
        this.displayHeight = displayHeight;
    }


    public interface HideShowSearchBarListener {
        void showSearchBar();

        void hideSearchBar();
    }
}
