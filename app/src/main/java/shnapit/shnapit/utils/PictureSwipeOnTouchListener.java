package shnapit.shnapit.utils;

import android.view.MotionEvent;
import android.view.View;

public class PictureSwipeOnTouchListener implements View.OnTouchListener {
    public static final int MINIMUM_SWIPE_DISTANCE = 50;

    private float scale, startX;
    private int minimumDistance, distance;

    private OnPictureSwipeListener onPictureSwipeListener;

    public PictureSwipeOnTouchListener(float scale) {
        this.scale = scale;
        this.minimumDistance = (int) (this.scale * MINIMUM_SWIPE_DISTANCE);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                break;

            case MotionEvent.ACTION_UP:
                distance = (int) (event.getX() - startX);

                if (distance <= -minimumDistance) {
                    if (onPictureSwipeListener != null)
                        onPictureSwipeListener.onDislike();
                    break;
                }

                if (distance >= minimumDistance && onPictureSwipeListener != null)
                    onPictureSwipeListener.onLike();
        }
        return true;
    }

    public void setOnPictureSwipeListener(OnPictureSwipeListener onPictureSwipeListener) {
        this.onPictureSwipeListener = onPictureSwipeListener;
    }

    public int getMinimumDistance() {
        return minimumDistance;
    }

    public interface OnPictureSwipeListener {
        void onLike();

        void onDislike();
    }
}
